package com.itheima.sms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @program: pinyougou_parent
 * @name: Application
 * @author;lin
 * @create: 2019-07-17 18:23
 * 这是一个启动类
 **/
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}




