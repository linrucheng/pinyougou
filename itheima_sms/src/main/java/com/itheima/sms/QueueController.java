package com.itheima.sms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: pinyougou_parent
 * @name: QueueController
 * @author;lin
 * @create: 2019-07-17 19:55
 * 配置生产者
 **/
@RestController
public class QueueController {
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    @RequestMapping("/sendMap")
    public void sendMap() {
        Map<String, String> map = new HashMap();
        map.put("phone", "18356024597");
        map.put("signName", "黑马林百万");
        map.put("templateCode", "SMS_170840048");
        map.put("templateParam", "{ \"code\":\"123456\"}");
        jmsMessagingTemplate.convertAndSend("itcast_map", map);
    }


}




