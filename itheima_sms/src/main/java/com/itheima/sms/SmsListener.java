package com.itheima.sms;

import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @program: pinyougou_parent
 * @name: SmsListener
 * @author;lin
 * @create: 2019-07-17 19:43
 * 消息监听类
 **/
@Component
public class SmsListener {
    @Autowired
    private SmsUtil smsUtil;

    @JmsListener(destination = "pinyougou_sms")
    public void readMessage(Map<String, String> map) {
        try {
            SendSmsResponse response = smsUtil.sendSms(map.get("phone"),
                    map.get("signName"),
                    map.get("templateCode"),
                    map.get("templateParam"));
            System.out.println("短信接口返回的数据----------------");
            System.out.println("Code=" + response.getCode());
            System.out.println("Message=" + response.getMessage());
            System.out.println("RequestId=" + response.getRequestId());
            System.out.println("BizId=" + response.getBizId());
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }


}




