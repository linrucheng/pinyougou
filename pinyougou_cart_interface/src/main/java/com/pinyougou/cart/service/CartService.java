package com.pinyougou.cart.service;

import com.pinyougou.pojoGroup.Cart;

import java.util.List;

public interface CartService {

    /**
     * 将item添加至购物车列表
     *
     * @param cartList
     * @param itemId
     * @param num
     * @return
     */
    public List<Cart> addItemToCartList(List<Cart> cartList, Long itemId, Integer num);

    /**
     * 从redis中获取购物车列表
     * @param username
     * @return
     */
    public List<Cart>findCartListFromRedis(String username);

    /**
     * 往redis中存购物车列表
     * @param username
     * @param cartList
     */
    public void saveCartListToRedis(String username,List<Cart> cartList);

    public List<Cart> mergeCarList(List<Cart>list1,List<Cart>list2);



}
