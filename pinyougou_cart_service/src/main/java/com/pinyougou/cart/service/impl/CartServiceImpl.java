package com.pinyougou.cart.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.pinyougou.cart.service.CartService;
import com.pinyougou.mapper.TbItemMapper;
import com.pinyougou.pojo.TbItem;
import com.pinyougou.pojo.TbOrderItem;
import com.pinyougou.pojoGroup.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: pinyougou_parent
 * @name: CartServiceImpl
 * @author;lin
 * @create: 2019-07-20 18:25
 **/
@Service
public class CartServiceImpl implements CartService {


    @Autowired
    private TbItemMapper itemMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 向购物车列表中添加商品
     *
     * @param cartList 购物车列表
     * @param itemId   skuId 只有一个
     * @param num      添加的数量
     * @return
     */
    @Override
    public List<Cart> addItemToCartList(List<Cart> cartList, Long itemId, Integer num) {
        //1根据itemId查询item
        TbItem item = itemMapper.selectByPrimaryKey(itemId);
        if (item == null) {
            throw new RuntimeException("商品不存在");
        }
        if (!"2".equals(item.getStatus())) {
            throw new RuntimeException("商品无效");
        }
        //2获得sellerId
        String sellerId = item.getSellerId();//商家id
        String seller = item.getSeller();//商家名称
        //3判断sellerId在购物车列表中假如不存在,即构建新的Cart,
        //购物车列表中有没有商家id,如果没有,创建一个购物车明细


        Cart cart = searchCartBySellerId(cartList, sellerId);
        if (cart == null) {
            cart = new Cart();
            cart.setSeller(seller);//商家名称
            cart.setSellerId(sellerId);//商家id
            // 创建新的orderItemList
            List<TbOrderItem> list = new ArrayList();
            TbOrderItem orderItem = createOrderItem(item, num);
            list.add(orderItem);
            cart.setOrderItemList(list);
            cartList.add(cart);//将一个购物车明细增加到购物车列表 cartList
        } else {
            //假如购物车列表中已经存在,则判断item在购物车明细中存不存在,如果存在修改数量和总金额,如果不存在,添加一个tbOrderItem
            //4sellerId在购物车列表中存在,在Cart的orderItemList判断
            //4.1假如存在,判断获得item是否在orderItemList中存在.

            List<TbOrderItem> orderItemList = cart.getOrderItemList();
            TbOrderItem tbOrderItem = searchTbOrderItemByItem(orderItemList, item);
            if (tbOrderItem == null) {
                //在购物车明细中没有该item,需要在orderItemList新增一个
                //4.3如果不存在,构建新的TbOrderItem
                tbOrderItem = createOrderItem(item, num);
                orderItemList.add(tbOrderItem);//新增购物车明细
            } else {
                //购物车明细中有该item,需要在数量上变化,总金额上变化
                //4.2如果存在,在数量上加num
                int i = tbOrderItem.getNum() + num;
                tbOrderItem.setNum(i);//变化后的数量
                tbOrderItem.setTotalFee(new BigDecimal(tbOrderItem.getPrice().doubleValue() * i));//变化后的价格
                if (i <= 0) {
                    //如果总数量小于等于0,需要从购物车明细表中移除tbOrderItem
                    orderItemList.remove(tbOrderItem);
                }
                if (orderItemList.size() <= 0) {
                    //如果移除后的购物车明细小于等于0,则在购物车列表移除次条记录
                    cartList.remove(cart);
                }
            }
        }
        return cartList;
    }


    /**
     * 判断item在购物车列表存不存在,存在返回cart,不存在返回null
     *
     * @param cartList
     * @param sellerId
     * @return
     */
    private Cart searchCartBySellerId(List<Cart> cartList, String sellerId) {
        for (Cart cart : cartList) {
            if (sellerId.equals(cart.getSellerId())) {
                return cart;
            }
        }
        return null;
    }

    //构建新的orderItem
    private TbOrderItem createOrderItem(TbItem item, Integer num) {
        if (num <= 0) {
            throw new RuntimeException("数据非法");
        }
        TbOrderItem tbOrderItem = new TbOrderItem();
        tbOrderItem.setItemId(item.getId());//商品id
        tbOrderItem.setGoodsId(item.getGoodsId());//SPU_ID
        tbOrderItem.setTitle(item.getTitle());//商品标题
        tbOrderItem.setPrice(item.getPrice());//商品价格
        tbOrderItem.setTotalFee(new BigDecimal(item.getPrice().doubleValue() * num));//商品总金额
        tbOrderItem.setPicPath(item.getImage());//商品图片地址
        tbOrderItem.setSellerId(item.getSellerId());//商家id
        tbOrderItem.setNum(num);//商品购买数量
        return tbOrderItem;
    }

    /**
     * 判断购物车明细中是否含有新添加的商品
     *
     * @param orderItemList
     * @param item
     * @return
     */

    private TbOrderItem searchTbOrderItemByItem(List<TbOrderItem> orderItemList, TbItem item) {
        for (TbOrderItem tbOrderItem : orderItemList) {
            if (tbOrderItem.getItemId().longValue() == item.getId().longValue()) {
                //假如有
                return tbOrderItem;
            }
        }
        return null;
    }

    /**
     * 从redis中存查数据
     *
     * @param username
     * @return
     */
    @Override
    public List<Cart> findCartListFromRedis(String username) {
        List<Cart> cartList = (List<Cart>) redisTemplate.boundHashOps("cartList").get(username);
        if (cartList == null) {
            cartList = new ArrayList<>();
        }
        return cartList;
    }

    /**
     * 往redis中存数据
     *
     * @param username
     * @param cartList
     */
    @Override
    public void saveCartListToRedis(String username, List<Cart> cartList) {
        redisTemplate.boundHashOps("cartList").put(username, cartList);
    }

    /**
     * 合并购物车
     *
     * @param list1
     * @param list2
     * @return
     */
    @Override
    public List<Cart> mergeCarList(List<Cart> list1, List<Cart> list2) {
        //遍历list2,得到每一个tbOrderItem,添加到list1
        for (Cart cart : list2) {
            for (TbOrderItem tbOrderItem : cart.getOrderItemList()) {
                addItemToCartList(list1, tbOrderItem.getItemId(), tbOrderItem.getNum());
            }
        }
        return list1;
    }



}




