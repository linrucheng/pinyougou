package com.pinyougou.cart.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.pinyougou.cart.service.CartService;
import com.pinyougou.entity.Result;
import com.pinyougou.pojoGroup.Cart;
import com.pinyougou.utils.CookieUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @program: pinyougou_parent
 * @name: CartController
 * @author;lin
 * @create: 2019-07-20 20:21
 **/
@RestController
@RequestMapping("/cart")
public class CartController {

    @Reference(timeout = 5000)
    private CartService cartService;

    @Autowired
    private HttpServletRequest request;
    @Autowired
    private HttpServletResponse response;


    /**
     * 从cookie中取List<Cart>
     *
     * @return
     */
    @RequestMapping("/findCartList")
    public List<Cart> findCartList() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();

        //用户没有登录,从cookie中取
        String cartListString = CookieUtil.getCookieValue(request, "cartList", "utf-8");

        if (cartListString == null || "".equals(cartListString)) {
            cartListString = "[]";
        }
        List<Cart> cartsCookie = JSON.parseArray(cartListString, Cart.class);
        if ("anonymousUser".equals(username)) {

            System.out.println("从cookie中获取");
            return cartsCookie;
        } else {
            //已经登录,从redis中取
            List<Cart> cartsRedis = cartService.findCartListFromRedis(username);
            System.out.println("从redis中取");
            if (cartsCookie.size() > 0) {
                //合并购物车
                List<Cart> cartList = cartService.mergeCarList(cartsRedis, cartsCookie);
                //清除cookie数据
                CookieUtil.deleteCookie(request, response, "cartList");
                //将合并后的购物车加到redis
                cartService.saveCartListToRedis(username, cartList);
                System.out.println("合并购物车");
                return cartList;
            }

            return cartsRedis;
        }


    }


    /**
     * 向购物车列表中添加
     *
     * @param itemId
     * @param num
     * @return 允许请求方式
     */
    @RequestMapping("/addItemToCartList")
    @CrossOrigin(origins = "http://localhost:9105", methods = {RequestMethod.POST, RequestMethod.GET})
    public Result addItemToCartList(Long itemId, Integer num) {
      /*  response.setHeader("Access-Control-Allow-Origin", "http://localhost:9105");
        response.setHeader("Access-Control-Allow-Credentials", "true");*/

        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            List<Cart> cartList = findCartList();
            List<Cart> carts = cartService.addItemToCartList(cartList, itemId, num);//更新后的购物车列表
            if ("anonymousUser".equals(username)) {

                String cartsStr = JSON.toJSONString(carts);
                //存入cookie
                CookieUtil.setCookie(request, response, "cartList", cartsStr, 60 * 60 * 24, "utf-8");
                System.out.println("向cookie存储");
            } else {
                //存入缓存中
                cartService.saveCartListToRedis(username, carts);//将购物车列表添加到redis
                System.out.println("向redis中存储");
            }
            return new Result(true, "存储成功");

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "存储失败");
        }
    }




}




