package com.pinyougou.cart.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.entity.Result;
import com.pinyougou.order.service.OrderService;
import com.pinyougou.pay.service.AlipayService;
import com.pinyougou.pay.service.WeixinPayService;
import com.pinyougou.pojo.TbPayLog;
import com.pinyougou.utils.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @program: pinyougou_parent
 * @name: PayController
 * @author;lin
 * @create: 2019-07-22 22:44
 **/
@RestController
@RequestMapping("/pay")
public class PayController {
    @Reference(timeout = 5000)
    private WeixinPayService weixinPayService;

    @Reference(timeout = 5000)
    private AlipayService alipayService;

    @Reference(timeout = 5000)
    private OrderService orderService;


    /**
     * 生成微信二维码的参数
     *
     * @return
     */
    @RequestMapping("/createWeiXingNative")
    public Map createWeiXingNative(String out_trade_no) {
        //得到out_trade_no
        System.out.println(out_trade_no);
        TbPayLog tbPayLog = orderService.selectPayLogFromRedis(out_trade_no);//从缓存中得到当前订单的支付日志
        if (tbPayLog != null) {
            Map map = weixinPayService.cerateNative(out_trade_no, tbPayLog.getTotalFee() + "");
            System.out.println(map);
            return map;
        } else {
            Map map = new HashMap();
            return map;
        }
    }


    @RequestMapping("/createAlipayNative")
    public Map createAlipayNative(String out_trade_no) {
        //1.获取当前登录用户
        //String username = SecurityContextHolder.getContext().getAuthentication().getName();
        //2.提取支付日志（从缓存 ）


        TbPayLog payLog = orderService.selectPayLogFromRedis(out_trade_no);//从缓存中提取支付日志
        //3.调用支付宝支付接口
        if (payLog != null) {
            return alipayService.createNative(out_trade_no, payLog.getTotalFee() + "");
        } else {
            return new HashMap<>();
        }
    }


    /**
     * 询问微信当前支付状态
     *
     * @param out_trade_no
     * @return
     */
    @RequestMapping("/queryWXPayStatus")
    public Result queryWXPayStatus(String out_trade_no) {
        Result result = new Result();
        int i = 0;
        while (true) {
            Map<String, String> map = weixinPayService.queryPayStatus(out_trade_no);
            if (map == null) {
                result = new Result(false, "支付出错");
                break;
            }
            if ("SUCCESS".equals(map.get("trade_state"))) {
                result = new Result(true, "支付成功");
                //支付成功后清除日志的缓存
                orderService.updateOrderStatus(out_trade_no, map.get("transaction_id"));
                break;
            }

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            i++;
            if (i >= 100) {
                result = new Result(false, "二维码超时");
                break;
            }

        }
        return result;

    }

    //http://itstu.natapp1.cc/pay/alipayCallBack.do
    //支付宝回调
    @RequestMapping("/alipayCallBack")
    public String alipayCallBack(HttpServletRequest request) {
        System.out.println("===================");
        System.out.println(request.getParameterMap());
        Map<String,String> params = new HashMap();
        Map requestParams = request.getParameterMap();
        for(Iterator iter = requestParams.keySet().iterator();iter.hasNext();){
            String name = (String)iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for(int i = 0 ; i <values.length;i++){

                valueStr = (i == values.length -1)?valueStr + values[i]:valueStr + values[i]+",";
            }
            params.put(name,valueStr);
        }
        if("TRADE_SUCCESS".equals(params.get("trade_status"))) {
            orderService.updateOrderStatus(params.get("out_trade_no")+"", params.get("trade_no"));//修改订单状态
        }
        return "success";
    }



    /**
     * 询问微信当前支付状态
     *
     * @param out_trade_no
     * @return
     */

    /**
     * 支付宝订单查询
     */
    @RequestMapping("/queryZFBPayStatus")
    public Result queryAliPayStatus(String out_trade_no) {
        Result result = null;
        int x = 0;
        while (true) {
            System.out.println("询问当前支付状态");
            System.out.println("等待支付");
            Map<String, String> map = alipayService.queryPayStatus(out_trade_no);//调用查询
            if (map != null) {
                result = new Result(true, "支付成功");
                System.out.println(map);
                break;
            }


            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            x++;
            if (x >= 100) {
                result = new Result(false, "二维码超时");
                break;
            }

        }
        return result;
    }


    ;


}






