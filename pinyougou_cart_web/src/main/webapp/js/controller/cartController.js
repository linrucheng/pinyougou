//控制层
app.controller('cartController', function ($scope, cartService, $controller, addressService) {
    $controller('baseController', {$scope: $scope});//继承


    //查询所有的购物车列表
    $scope.findCartList = function () {

        cartService.findCartList().success(
            function (response) {
                $scope.CartList = response;
                $scope.total = cartService.sum($scope.CartList)
            })
    }


    //添加商品
    $scope.addItemToCartList = function (itemId, num) {
        cartService.addItemToCartList(itemId, num).success(
            function (response) {
                $scope.findCartList();
            }
        )
    }
    //查询登录的地址列表
    $scope.findAddressList = function () {

        cartService.findAddressList().success(
            function (response) {
                $scope.addressList = response;
                for (var i = 0; i < $scope.addressList.length; i++) {
                    if ($scope.addressList[i].isDefault == '1') {
                        $scope.address = $scope.addressList[i];
                        break;
                    }

                }

            })
    }
    //用户选中的选择地址
    $scope.selectAddress = function (address) {
        $scope.address = address;
    }
    //判断地址有没有被选中
    $scope.isSelected = function (address) {
        if ($scope.address == address) {
            return true;
        } else {
            return false;
        }
    }

    //保存
    $scope.save = function () {
        var serviceObject;//服务层对象
        if ($scope.entity.id != null) {//如果有ID
            serviceObject = addressService.update($scope.entity); //修改
        } else {
            serviceObject = addressService.add($scope.entity);//增加
        }
        serviceObject.success(
            function (response) {
                if (response.success) {
                    //重新查询
                    $scope.findAddressList()
                } else {
                    alert(response.message)
                }
            }
        );
    }

    //查询实体
    $scope.findOne = function (id) {
        addressService.findOne(id).success(
            function (response) {
                $scope.entity = response;
            }
        );
    }


    //批量删除
    $scope.dele = function (id) {
        //获取选中的复选框
        addressService.dele(id).success(
            function (response) {
                if (response.success) {

                    $scope.findAddressList();//刷新列表
                } else {
                    alert("删除失败")
                }
            }
        );
    }
    //选择支付方式1微信在线支付,2货到付款
    $scope.order = {paymentType: '1'}
    $scope.selectPayType = function (type) {
        $scope.order.paymentType = type;
    }
    //提交订单

    $scope.addOrder = function () {
        $scope.order.receiverAreaName = $scope.address.address//联系人地址
        $scope.order.receiverMobile = $scope.address.mobile//联系人电话
        $scope.order.receiver = $scope.address.contact//联系人
        cartService.addOrder($scope.order).success(function (response) {
            if (response.success) {
                //alert(response.out_trade_no)
                if ($scope.order.paymentType != "3") {//在线支付,传递订单号和支付方式
                    location.href = "pay.html#?out_trade_no=" + response.out_trade_no + "&paymentType=" + $scope.order.paymentType;
                }




                else {
                    location.href = "paysuccess.html"//其他方式支付
                }
            } else {
                alert("支付失败")
            }
        })
    }


    $scope.selectCartList = []

    $scope.addToCarList = function ($event, pojo) {
        if ($event.target.checked) {

            $scope.selectCartList.push(pojo);
        } else {
            //获取将id数组中移除
            //获取id的索引
            var indexId = $scope.selectCartList.indexOf(pojo);
            $scope.selectCartList.splice(indexId, 1);
        }
    }

    //添加方法,获得选中的物品


})



