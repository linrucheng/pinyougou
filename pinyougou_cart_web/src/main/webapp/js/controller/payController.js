app.controller('payController', function ($scope, payService, $location) {

    //获得结算页提交的支付订单号out_trade_no和支付方式
    getOutTradeNo = function () {
        return $location.search()['out_trade_no']
    }
    getpaymentType = function () {
        return $location.search()['paymentType'];//支付方式
    }


    $scope.createNative = function () {
        // alert(getpaymentType())
        // alert(getOutTradeNo())
        $scope.payType = getpaymentType();

        if (getpaymentType() == '1') {//微信支付
            payService.createWeiXingNative(getOutTradeNo()).success(
                function (response) {
                    $scope.money = (response.total_fee / 100).toFixed(2); //金额
                    $scope.out_trade_no = response.out_trade_no;//订单号
                    //二维码
                    var qr = new QRious({
                        element: document.getElementById('qrious'),
                        size: 250,
                        level: 'H',
                        value: response.code_url
                    });
                    queryWXPayStatus($scope.out_trade_no)
                }
            );
        }
        if (getpaymentType() == "2") {
            payService.createAlipayNative(getOutTradeNo()).success(
                function (response) {
                    $scope.money = (response.total_fee / 100).toFixed(2); //金额
                    $scope.out_trade_no = response.out_trade_no;//订单号
                    //二维码
                    var qr = new QRious({
                        element: document.getElementById('qrious'),
                        size: 250,
                        level: 'H',
                        value: response.code_url
                    });
                    queryZFBPayStatus($scope.out_trade_no)
                }
            );
        }


    }
    //查询微信状态
    queryWXPayStatus = function (out_trade_no) {
        payService.queryXMPayStatus(out_trade_no).success(
            function (response) {
                if (response.success) {
                    location.href = "paysuccess.html#?money=" + $scope.money;//发送给支付成功页面发送当前的支付金额
                } else {
                    if (response.message = '二维码超时') {
                        $scope.createNative()
                    } else {
                        location.href = "payfail.html"
                    }
                }
            })

    }
    //查询支付宝支付状态
    queryZFBPayStatus = function (out_trade_no) {
        payService.queryZFBPayStatus(out_trade_no).success(
            function (response) {
                if (response.success) {
                    location.href = "paysuccess.html#?money=" + $scope.money;//发送给支付成功页面发送当前的支付金额
                } else {
                    if (response.message = '二维码超时') {
                        $scope.createNative()
                    } else {
                        location.href = "payfail.html"
                    }
                }
            })

    }


    //获得当前支付的金额
    $scope.getTotalMoney = function () {
        return $location.search()['money']//接收页面传来的金额
    }


});



