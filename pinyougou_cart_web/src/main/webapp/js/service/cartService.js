//服务层
app.service('cartService', function ($http) {
    //
    this.findCartList = function () {

        return $http.get('/cart/findCartList.do')
    }
    //添加商品

    this.addItemToCartList = function (itemId, num) {
        return $http.get('cart/addItemToCartList.do?itemId=' + itemId + '&num=' + num)
    }
    //求合计的总金额和总数量
    this.sum = function (cartList) {
        var totalValue = {totalNum: 0, totalMoney: 0.00}//总金额和总数量
        for (var i = 0; i < cartList.length; i++) {
            var list = cartList[i].orderItemList
            for (var j = 0; j < list.length; j++) {
                totalValue.totalNum += list[j].num
                totalValue.totalMoney += list[j].totalFee;
            }
        }
        return totalValue;
    }

    /**
     * 查询用户的所有地址
     */
    this.findAddressList = function () {
        return $http.get('../address/findAddressByUid.do')
    }

    //提交订单

    this.addOrder = function (order) {
        return $http.post('../order/add.do', order);
    }


})