app.service('payService', function ($http) {
    this.createWeiXingNative = function (out_trade_no) {
        return $http.get("../pay/createWeiXingNative.do?out_trade_no=" + out_trade_no)
    }

    this.createAlipayNative = function (out_trade_no) {
        return $http.get("../pay/createAlipayNative.do?out_trade_no=" + out_trade_no)
    }


    this.queryXMPayStatus = function (out_trade_no) {
        return $http.get("../pay/queryWXPayStatus.do?out_trade_no=" + out_trade_no)
    }

    this.queryZFBPayStatus = function (out_trade_no) {
        return $http.get("../pay/queryZFBPayStatus.do?out_trade_no=" + out_trade_no)
    }


})