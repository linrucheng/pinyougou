package com.pinyougou.content.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.pinyougou.mapper.TbContentMapper;
import com.pinyougou.pojo.TbContent;
import com.pinyougou.pojo.TbContentExample;
import com.pinyougou.pojo.TbContentExample.Criteria;
import com.pinyougou.content.service.ContentService;

import com.pinyougou.entity.PageResult;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;


/**
 * 服务实现层
 *
 * @author Administrator
 */
@Transactional
@Service
public class ContentServiceImpl implements ContentService {

    @Autowired
    private TbContentMapper contentMapper;
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 查询全部
     */
    @Override
    public List<TbContent> findAll() {
        return contentMapper.selectByExample(null);
    }

    /**
     * 按分页查询
     */
    @Override
    public PageResult findPage(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        Page<TbContent> page = (Page<TbContent>) contentMapper.selectByExample(null);
        return new PageResult(page.getTotal(), page.getResult());
    }

    /**
     * 增加
     */
    @Override
    public void add(TbContent content) {

        contentMapper.insert(content);
        //新增广告后清除缓存
        redisTemplate.boundHashOps("content").delete(content.getCategoryId());

    }


    /**
     * 修改
     */
    @Override
    public void update(TbContent content) {//新的数据

        //取分类id
        Long categoryId = contentMapper.selectByPrimaryKey(content.getId()).getCategoryId();
        if (categoryId.longValue() == content.getCategoryId().longValue()) {
            //假如没有修改分类id
            redisTemplate.boundHashOps("content").delete(categoryId);
        } else {
            //假如不一样,则清除两个分类id的缓存
            redisTemplate.boundHashOps("content").delete(categoryId);
            redisTemplate.boundHashOps("content").delete(content.getCategoryId());
        }
        contentMapper.updateByPrimaryKey(content);
    }


    /**
     * 根据ID获取实体
     *
     * @param id
     * @return
     */
    @Override
    public TbContent findOne(Long id) {
        return contentMapper.selectByPrimaryKey(id);
    }

    /**
     * 批量删除
     */
    @Override
    public void delete(Long[] ids) {
        for (Long id : ids) {
            TbContent tbContent = contentMapper.selectByPrimaryKey(id);

            contentMapper.deleteByPrimaryKey(id);
            redisTemplate.boundHashOps("content").delete(tbContent.getCategoryId());


        }
    }


    @Override
    public PageResult findPage(TbContent content, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);

        TbContentExample example = new TbContentExample();
        Criteria criteria = example.createCriteria();

        if (content != null) {
            if (content.getTitle() != null && content.getTitle().length() > 0) {
                criteria.andTitleLike("%" + content.getTitle() + "%");
            }
            if (content.getUrl() != null && content.getUrl().length() > 0) {
                criteria.andUrlLike("%" + content.getUrl() + "%");
            }
            if (content.getPic() != null && content.getPic().length() > 0) {
                criteria.andPicLike("%" + content.getPic() + "%");
            }
            if (content.getStatus() != null && content.getStatus().length() > 0) {
                criteria.andStatusLike("%" + content.getStatus() + "%");
            }

        }

        Page<TbContent> page = (Page<TbContent>) contentMapper.selectByExample(example);
        return new PageResult(page.getTotal(), page.getResult());
    }

    /**
     * 根据广告类id,查询所有的广告
     *
     * @param categoryId
     * @return
     */
    @Override
    public List<TbContent> findByCategoryId(Long categoryId) {
        /*存储的是map格式 即map<categoryId,List<>>*/
        //先从缓存里面取
        List<TbContent> contents = (List<TbContent>) redisTemplate.boundHashOps("content").get(categoryId);
        if (contents == null) {
            //缓存里面没有,从数据库里取
            TbContentExample example = new TbContentExample();
            Criteria criteria = example.createCriteria();
            criteria.andCategoryIdEqualTo(categoryId);
            criteria.andStatusEqualTo("1");//广告状态开启
            example.setOrderByClause("sort_order desc");//排序 desc表示降续
            contents = contentMapper.selectByExample(example);
            redisTemplate.boundHashOps("content").put(categoryId, contents);

           /* //防止出现缓存穿透,从数据库查询的值为空,放入缓存,取出依然为null,然后再次查询数据库,对数据库造成压力
            if (contents != null&&contents.size()>0) {
                //存入缓存

            } else {
                //放入缓存一个null,防止短时间一直访问数据库

                redisTemplate.boundHashOps("content").put(categoryId, null);

            }
*/
        } else {
            System.out.println("从缓存中取数据");
        }


        return contents;


    }

}
