package com.pinyougou.manager.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.entity.PageResult;
import com.pinyougou.entity.Result;
import com.pinyougou.pojo.TbBrand;
import com.pinyougou.sellergoods.service.BrandService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @program: pinyougou_parent
 * @name: BrandController
 * @author;lin
 * @create: 2019-06-28 13:02
 **/
@RestController
@RequestMapping("/brand")
public class BrandController {
    @Reference
    private BrandService brandService;


    @RequestMapping("/findAll.do")
    public List<TbBrand> findAll() {
        //查询所有
        List<TbBrand> list = brandService.findAll();
        return list;
    }

    @RequestMapping("/findByPage.do")
    public PageResult findByPage(int currentPage, int pageSize) {
        //分页显示 currentPage当前页  pageSize 每页记录数
        PageResult result = brandService.findByPage(currentPage, pageSize);
        return result;
    }

    @RequestMapping("/add.do")
    public Result add(@RequestBody TbBrand tbBrand) {
        //品牌的增加RequestBody注解使用是必须要有请求体,即post请求
        Result result = new Result();
        try {
            brandService.add(tbBrand);
            result.setSuccess(true);
            result.setMessage("添加成功");
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            result.setSuccess(false);
            result.setMessage("添加失败");
            return result;
        }
    }

    @RequestMapping("/findOne.do")
    public TbBrand findOne(Long id) {
        //根据id查询一个
        return brandService.findOne(id);
    }

    @RequestMapping("/update.do")
    public Result update(@RequestBody TbBrand tbBrand) {
        //RequestBody将请求参数封装为TbBrand对象
        Result result = new Result();
        try {
            brandService.update(tbBrand);
            result.setSuccess(true);
            result.setMessage("修改成功");
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            result.setSuccess(false);
            result.setMessage("修改失败");
            return result;
        }
    }

    @RequestMapping("/delete.do")
    public Result delete(Long[] ids) {
        //删除
        Result result = new Result();
        try {
            brandService.delete(ids);
            result.setSuccess(true);
            result.setMessage("删除成功");
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            result.setSuccess(false);
            result.setMessage("删除失败");
            return result;
        }
    }

    @RequestMapping("/findByPageAndCondition.do")
    public PageResult findByPageAndCondition(@RequestBody TbBrand tbBrand, int currentPage, int pageSize) {
        PageResult page = brandService.findByPage(currentPage, pageSize, tbBrand);
        return page;
    }



    @RequestMapping("/selectBrandList.do")
    public List<Map> selectBrandList() {
        return brandService.selectBrandList();
    }



}







