package com.pinyougou.manager.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.alibaba.fastjson.JSON;

import com.pinyougou.pojo.TbItem;
import com.pinyougou.pojoGroup.Goods;

import com.pinyougou.sellergoods.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pojo.TbGoods;
import com.pinyougou.sellergoods.service.GoodsService;

import com.pinyougou.entity.Result;
import com.pinyougou.entity.PageResult;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

/**
 * controller
 *
 * @author Administrator
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Reference
    private GoodsService goodsService;
    @Reference
    private ItemService itemService;
    /*@Reference(timeout = 5000)
    private ItemSearchService itemSearchService;*/
    @Autowired
    private Destination queueSolrtDestination;
    @Autowired
    private JmsTemplate jmsTemplate;
    @Autowired
    private Destination topicObjectDestination;//生成静态页面

    @Autowired
    private Destination topicObjectDeleDestination;//删除静态页面


    /**
     * 返回全部列表
     *
     * @return
     */
    @RequestMapping("/findAll")
    public List<TbGoods> findAll() {
        return goodsService.findAll();
    }


    /**
     * 返回全部列表
     *
     * @return
     */
    @RequestMapping("/findPage")
    public PageResult findPage(int page, int rows) {
        return goodsService.findPage(page, rows);
    }


    /**
     * 修改
     *
     * @param goods
     * @return
     */
    @RequestMapping("/update")
    public Result update(@RequestBody Goods goods) {


        try {
            goodsService.update(goods);
            return new Result(true, "修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "修改失败");
        }
    }

    /**
     * 获取实体
     *
     * @param id
     * @return
     */
    @RequestMapping("/findOne")
    public Goods findOne(Long id) {
        return goodsService.findOne(id);
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @Autowired
    private Destination queueSolrtDeleteDestination;

    @RequestMapping("/delete")
    public Result delete(Long[] ids) {
        List list = new ArrayList();//审核通过的商品id
        try {
            List<TbGoods> tbGoods = goodsService.findByIds(ids);
            for (TbGoods tbGood : tbGoods) {
                //判断是否是审核通过的商品
                if ("2".equals(tbGood.getAuditStatus())) {
                    list.add(tbGood.getId());
                }
            }
            // itemSearchService.deleteByGoodIds(list);
            //点对点,发送需要删除的list集合,删除索引库
            final String jsonString = JSON.toJSONString(list);
            jmsTemplate.send(queueSolrtDeleteDestination, new MessageCreator() {
                @Override
                public Message createMessage(Session session) throws JMSException {
                    return session.createTextMessage(jsonString);
                }
            });
            //删除静态页面
            jmsTemplate.send(topicObjectDeleDestination, new MessageCreator() {
                @Override
                public Message createMessage(Session session) throws JMSException {
                    return session.createTextMessage(jsonString);
                }
            });


            goodsService.delete(ids);
            return new Result(true, "删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "删除失败");
        }
    }

    /**
     * 查询+分页
     *
     * @param
     * @param page
     * @param rows
     * @return
     */
    @RequestMapping("/search")
    public PageResult search(@RequestBody TbGoods goods, int page, int rows) {
        return goodsService.findPage(goods, page, rows);
    }


    /**
     * 修改状态
     *
     * @param ids
     * @param status
     * @return
     */


    @RequestMapping("/updateStatus")
    public Result updateStatus(Long[] ids, String status) {
        Set<Long> set = new HashSet();
        try {
            goodsService.updateStatus(ids, status);
            itemService.updateStatus(ids, status);
            //根据id和状态为2的从数据库查询.
            if ("2".equals(status)) {
                List<TbItem> itemList = itemService.findByIdsAndStatus(ids, status);
                //数据批量导入
                if (itemList != null && itemList.size() > 0) {
                    //itemSearchService.saveItemList(itemList);
                    //将数据转化为字符串
                    final String itemListStr = JSON.toJSONString(itemList);
                    //向消息中间件发送消息
                    jmsTemplate.send(queueSolrtDestination, new MessageCreator() {
                        @Override
                        public Message createMessage(Session session) throws JMSException {
                            return session.createTextMessage(itemListStr);
                        }
                    });


                }
                /*审核通过的goodsid*/
                for (TbItem item : itemList) {
                    Long goodsId = item.getGoodsId();
                    set.add(goodsId);
                }

                for (final Long goodsId : set) {
                    jmsTemplate.send(topicObjectDestination, new MessageCreator() {
                        @Override
                        public Message createMessage(Session session) throws JMSException {
                            return session.createObjectMessage(goodsId);//Long实现序列化接口
                        }
                    });
                }


              /*  //在商品审核通过生成静态页面
                for (TbItem item : itemList) {
                    final Long goodsId = item.getGoodsId();
                    //itemPageService.getItemHtml(goodsId);
                    jmsTemplate.send(topicObjectDestination, new MessageCreator() {
                        @Override
                        public Message createMessage(Session session) throws JMSException {
                            return session.createObjectMessage(goodsId);//Long实现序列化接口
                        }
                    });


                }*/
            }


            return new Result(true, "操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "操作失败");
        }
    }

    /*@Reference(timeout = 10000)
    private ItemPageService itemPageService;*/

/*
    @RequestMapping("/getHtml")
    public Result getHtml(Long goodsId) {
        itemPageService.getItemHtml(goodsId);
        return new Result(true, "操作成功");
    }
*/


}
