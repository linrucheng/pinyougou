package com.pinyougou.manager.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: pinyougou_parent
 * @name: LoginController
 * @author;lin
 * @create: 2019-07-02 18:27
 * 和登录相关
 **/
@RequestMapping("/login")
@RestController
public class LoginController {


    @RequestMapping("/getUsername.do")
    public Map getUsername() {
        //获取登录用户名
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Map map = new HashMap();
        map.put("username", username);
        return map;
    }

}




