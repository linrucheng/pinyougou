package com.pinyougou.manager.controller;

import com.pinyougou.entity.Result;
import com.pinyougou.utils.FastDFSClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @program: pinyougou_parent
 * @name: UploadController
 * @author;lin
 * @create: 2019-07-04 21:00
 * 文件上传
 **/
@RestController
public class UploadController {
    @Value("${FILE_SERVER_URL}") //使用EL表达式读取配置文件并赋值
    private String FILE_SERVER_URL;

    @RequestMapping("/upload")
    public Result upload(MultipartFile file) {


        /**
         *  创建fastDfs客户端指定配置文件位置
         *
         */
        try {
            //获取文件拓展名
            String filename = file.getOriginalFilename();
            String extName = filename.substring(filename.lastIndexOf(".") + 1);
            FastDFSClient fastDFSClient = new FastDFSClient("classpath:config/fdfs_client.conf");

            //执行文件上传 第一个参数为字节数组,第二个参数为文件扩展名
            String path = fastDFSClient.uploadFile(file.getBytes(), extName);
            //拼接成URL
            String url = FILE_SERVER_URL + path;
            return new Result(true, url);//返回上传成功的fileId,即在文件服务器的位置
        } catch (Exception e) {

            return new Result(false, "上传失败");
        }
    }
}










