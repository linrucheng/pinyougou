package com.pinyougou.order.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.alibaba.dubbo.common.json.JSON;
import com.alibaba.dubbo.config.annotation.Service;
import com.pinyougou.mapper.TbOrderItemMapper;
import com.pinyougou.mapper.TbPayLogMapper;
import com.pinyougou.order.service.OrderService;
import com.pinyougou.pojo.*;
import com.pinyougou.pojoGroup.Cart;
import com.pinyougou.utils.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.pinyougou.mapper.TbOrderMapper;
import com.pinyougou.pojo.TbOrderExample.Criteria;

import com.pinyougou.entity.PageResult;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;


/**
 * 服务实现层
 *
 * @author Administrator
 */
@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    @Autowired
    private TbOrderMapper orderMapper;
    @Autowired
    private TbOrderItemMapper tbOrderItemMapper;

    /**
     * 查询全部
     */
    @Override
    public List<TbOrder> findAll() {
     /*   TbOrderItemExample exampl;
        tbOrderItemMapper.selectByExample(exampl);*/


        return orderMapper.selectByExample(null);
    }

    /**
     * 按分页查询
     */
    @Override
    public PageResult findPage(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        Page<TbOrder> page = (Page<TbOrder>) orderMapper.selectByExample(null);
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private IdWorker idWorker;

    @Autowired
    private TbPayLogMapper tbPayLogMapper;

    /**
     * @param order 一个商家的商品保存一个订单
     *              返回生成的
     */
    @Override
    @Transactional
    public String add(TbOrder order) {
        //从缓存中读取购物车明细,
        double payment = 0;//初始化支付金额
        List<Cart> cartList = (List<Cart>) redisTemplate.boundHashOps("cartList").get(order.getUserId());
        List order_list = new ArrayList();//订单编号
        for (Cart cart : cartList) {//得到一个商家的商品对应一个购物车,
            long orderId = idWorker.nextId();//订单编号
            System.out.println("订单id" + orderId);
            TbOrder tbOrder = new TbOrder();
            tbOrder.setOrderId(orderId);//订单id
            tbOrder.setPaymentType(order.getPaymentType());//支付类型
            tbOrder.setStatus("1");//状态1,未付款
            tbOrder.setCreateTime(new Date());//订单创建时间
            tbOrder.setUpdateTime(new Date());//订单修改时间
            tbOrder.setUserId(order.getUserId());//用户id
            tbOrder.setReceiverAreaName(order.getReceiverAreaName());//收货人地区名称
            tbOrder.setReceiverMobile(order.getReceiverMobile());//收货人号码
            tbOrder.setReceiver(order.getReceiver());//收货人姓名
            tbOrder.setSourceType(order.getSourceType());//订单来源
            tbOrder.setSellerId(cart.getSellerId());//商家id


            for (TbOrderItem orderItem : cart.getOrderItemList()) {
                orderItem.setId(idWorker.nextId());//订单明细id
                orderItem.setOrderId(orderId);//订单编号
                payment += orderItem.getTotalFee().doubleValue();
                tbOrderItemMapper.insert(orderItem);
            }
            tbOrder.setPayment(new BigDecimal(payment));//支付金额
            orderMapper.insert(tbOrder);
            order_list.add(orderId);

        }
        //如果是微信支付
        String out_trade_no = idWorker.nextId() + "";//支付订单号
        if ("1".equals(order.getPaymentType())) {
            //生成支付日志

            TbPayLog tbPayLog = new TbPayLog();
            tbPayLog.setOutTradeNo(out_trade_no);//支付订单号
            tbPayLog.setCreateTime(new Date());//创建时间
            tbPayLog.setTotalFee((long) (payment * 100));//支付总金额(分)
            tbPayLog.setUserId(order.getUserId());//用户id
            tbPayLog.setTradeState("0");//交易状态(0未支付,1已支付)
            tbPayLog.setPayType(order.getPaymentType());//支付类型
            //订单编号order_list
            String order_list_str = com.alibaba.fastjson.JSON.toJSONString(order_list);
            String orderList = order_list_str.replace("[", "").replace("]", "");//将订单号替换
            tbPayLog.setOrderList(orderList);//订单编号列表
            tbPayLogMapper.insert(tbPayLog);//插入日志表
            System.out.println("向数据库插入支付日志" + tbPayLog);
            redisTemplate.boundHashOps("payLog").put(out_trade_no, tbPayLog);
        }
        if ("2".equals(order.getPaymentType())){//如果是支付宝支付
            //生成支付日志

            TbPayLog tbPayLog = new TbPayLog();
            tbPayLog.setOutTradeNo(out_trade_no);//支付订单号
            tbPayLog.setCreateTime(new Date());//创建时间
            tbPayLog.setTotalFee((long) (payment * 100));//支付总金额(分)
            tbPayLog.setUserId(order.getUserId());//用户id
            tbPayLog.setTradeState("0");//交易状态(0未支付,1已支付)
            tbPayLog.setPayType(order.getPaymentType());//支付类型
            //订单编号order_list
            String order_list_str = com.alibaba.fastjson.JSON.toJSONString(order_list);
            String orderList = order_list_str.replace("[", "").replace("]", "");//将订单号替换
            tbPayLog.setOrderList(orderList);//订单编号列表
            tbPayLogMapper.insert(tbPayLog);//插入日志表
            System.out.println("向数据库插入支付日志" + tbPayLog);
            redisTemplate.boundHashOps("payLog").put(out_trade_no, tbPayLog);
        }







        //清除缓存-购物车
        redisTemplate.boundHashOps("cartList").delete(order.getUserId());

        return out_trade_no;


    }


    /**
     * 修改
     */
    @Override
    public void update(TbOrder order) {
        orderMapper.updateByPrimaryKey(order);
    }

    /**
     * 根据ID获取实体
     *
     * @param id
     * @return
     */
    @Override
    public TbOrder findOne(Long id) {
        return orderMapper.selectByPrimaryKey(id);
    }

    /**
     * 批量删除
     */
    @Override
    public void delete(Long[] ids) {
        for (Long id : ids) {
            orderMapper.deleteByPrimaryKey(id);
        }
    }


    @Override
    public PageResult findPage(TbOrder order, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);

        TbOrderExample example = new TbOrderExample();
        Criteria criteria = example.createCriteria();

        if (order != null) {
            if (order.getPaymentType() != null && order.getPaymentType().length() > 0) {
                criteria.andPaymentTypeLike("%" + order.getPaymentType() + "%");
            }
            if (order.getPostFee() != null && order.getPostFee().length() > 0) {
                criteria.andPostFeeLike("%" + order.getPostFee() + "%");
            }
            if (order.getStatus() != null && order.getStatus().length() > 0) {
                criteria.andStatusLike("%" + order.getStatus() + "%");
            }
            if (order.getShippingName() != null && order.getShippingName().length() > 0) {
                criteria.andShippingNameLike("%" + order.getShippingName() + "%");
            }
            if (order.getShippingCode() != null && order.getShippingCode().length() > 0) {
                criteria.andShippingCodeLike("%" + order.getShippingCode() + "%");
            }
            if (order.getUserId() != null && order.getUserId().length() > 0) {
                criteria.andUserIdLike("%" + order.getUserId() + "%");
            }
            if (order.getBuyerMessage() != null && order.getBuyerMessage().length() > 0) {
                criteria.andBuyerMessageLike("%" + order.getBuyerMessage() + "%");
            }
            if (order.getBuyerNick() != null && order.getBuyerNick().length() > 0) {
                criteria.andBuyerNickLike("%" + order.getBuyerNick() + "%");
            }
            if (order.getBuyerRate() != null && order.getBuyerRate().length() > 0) {
                criteria.andBuyerRateLike("%" + order.getBuyerRate() + "%");
            }
            if (order.getReceiverAreaName() != null && order.getReceiverAreaName().length() > 0) {
                criteria.andReceiverAreaNameLike("%" + order.getReceiverAreaName() + "%");
            }
            if (order.getReceiverMobile() != null && order.getReceiverMobile().length() > 0) {
                criteria.andReceiverMobileLike("%" + order.getReceiverMobile() + "%");
            }
            if (order.getReceiverZipCode() != null && order.getReceiverZipCode().length() > 0) {
                criteria.andReceiverZipCodeLike("%" + order.getReceiverZipCode() + "%");
            }
            if (order.getReceiver() != null && order.getReceiver().length() > 0) {
                criteria.andReceiverLike("%" + order.getReceiver() + "%");
            }
            if (order.getInvoiceType() != null && order.getInvoiceType().length() > 0) {
                criteria.andInvoiceTypeLike("%" + order.getInvoiceType() + "%");
            }
            if (order.getSourceType() != null && order.getSourceType().length() > 0) {
                criteria.andSourceTypeLike("%" + order.getSourceType() + "%");
            }
            if (order.getSellerId() != null && order.getSellerId().length() > 0) {
                criteria.andSellerIdLike("%" + order.getSellerId() + "%");
            }

        }

        Page<TbOrder> page = (Page<TbOrder>) orderMapper.selectByExample(example);
        return new PageResult(page.getTotal(), page.getResult());
    }


    /**
     * 根据支付订单从缓存中获得支付日志
     *
     * @param out_trade_no
     * @return
     */
    @Override
    public TbPayLog selectPayLogFromRedis(String out_trade_no) {
        return (TbPayLog) redisTemplate.boundHashOps("payLog").get(out_trade_no);
    }

    /**
     * 付款成功修改日志表tb_pay_log和订单表tb_order
     *
     * @param out_trade_no   支付订单号
     * @param transaction_id 交易号码
     */
    @Override
    public void updateOrderStatus(String out_trade_no, String transaction_id) {
        //1修改日志的状态
        TbPayLog tbPayLog = tbPayLogMapper.selectByPrimaryKey(out_trade_no);
        tbPayLog.setTradeState("1");//已支付
        tbPayLog.setPayTime(new Date());//支付时间
        tbPayLog.setTransactionId(transaction_id);//交易号码
        tbPayLogMapper.updateByPrimaryKeySelective(tbPayLog);//更新
        //2修改订单的状态
        String orderListStr = tbPayLog.getOrderList();//订单的集合.格式12,34
        String[] orderList = orderListStr.split(",");
        for (String orderId : orderList) {
            TbOrder tbOrder = orderMapper.selectByPrimaryKey(Long.valueOf(orderId));//获得tbOrder
            tbOrder.setStatus("2");//已付款
            orderMapper.updateByPrimaryKeySelective(tbOrder);//更新
        }
        //3清除redis缓存的日志
        redisTemplate.boundHashOps("payLog").delete(out_trade_no);
        System.out.println("情况redis缓存");
    }


}
