package com.pinyougou.page.service;

import java.util.List;

/**
 * 商品详细页接口
 */

public interface ItemPageService {

    /**
     * 生成商品详情页
     * @return
     */
    public Boolean getItemHtml(Long goodId);

    /**
     * 删除静态页面
     * @param goodIds
     * @return
     */
    public Boolean deleItemHtml(List goodIds);


}
