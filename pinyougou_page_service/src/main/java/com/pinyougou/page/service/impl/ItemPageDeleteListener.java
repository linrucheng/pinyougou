package com.pinyougou.page.service.impl;

import com.alibaba.fastjson.JSON;
import com.pinyougou.page.service.ItemPageService;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.*;
import java.sql.Array;
import java.util.Collections;
import java.util.List;

/**
 * @program: pinyougou_parent
 * @name: ItemPageListener
 * @author;lin
 * @create: 2019-07-16 20:49
 **/
@Component
public class ItemPageDeleteListener implements MessageListener {

    @Autowired
    private ItemPageService itemPageService;

    @Override
    public void onMessage(Message message) {
        //将参数强转为text类型
        TextMessage textMessage = (TextMessage) message;
        try {
            String itemListStr = textMessage.getText();
            System.out.println("接收到消息" + itemListStr);
            //将字符串转为list集合
            List list = JSON.parseArray(itemListStr);
            //删除静态页面
            itemPageService.deleItemHtml(list);


        } catch (JMSException e) {
            e.printStackTrace();
        }
    }


}






