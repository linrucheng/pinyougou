package com.pinyougou.page.service.impl;

import com.pinyougou.page.service.ItemPageService;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.io.Serializable;

/**
 * @program: pinyougou_parent
 * @name: ItemPageListener
 * @author;lin
 * @create: 2019-07-16 20:49
 **/
@Component
public class ItemPageListener implements MessageListener {
    @Autowired
    private ActiveMQTopic topicObjectDestination;
    @Autowired
    private ItemPageService itemPageService;

    @Override
    public void onMessage(Message message) {
        //将参数强转为Object类型
        ObjectMessage objectMessage = (ObjectMessage) message;
        try {
            Long goodsId = (Long) objectMessage.getObject();
            Boolean b = itemPageService.getItemHtml(goodsId);
            System.out.println(b);


        } catch (JMSException e) {
            e.printStackTrace();
        }


    }

}




