package com.pinyougou.page.service.impl;


import com.pinyougou.mapper.TbGoodsDescMapper;
import com.pinyougou.mapper.TbGoodsMapper;
import com.pinyougou.mapper.TbItemCatMapper;
import com.pinyougou.mapper.TbItemMapper;
import com.pinyougou.page.service.ItemPageService;
import com.pinyougou.pojo.*;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.apache.commons.io.output.FileWriterWithEncoding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: pinyougou_parent
 * @name: ItemPageServiceImpl
 * @author;lin
 * @create: 2019-07-14 18:11
 **/
@Service
public class ItemPageServiceImpl implements ItemPageService {

    @Value("${pagedir}")
    private String pagedir;//指定要生成page的位置
    @Autowired
    private FreeMarkerConfigurer markerConfigurer;//指定生成的位置
    @Autowired
    private TbGoodsMapper tbGoodsMapper;
    @Autowired
    private TbGoodsDescMapper tbGoodsDescMapper;
    @Autowired
    private TbItemCatMapper tbItemCatMapper;
    @Autowired
    private TbItemMapper tbItemMapper;


    @Override
    public Boolean getItemHtml(Long goodsId) {
        //创建配置类
        Configuration configuration = markerConfigurer.getConfiguration();
        //加载模板
        try {
            Template template = configuration.getTemplate("item.ftl");
            Map dateModel = new HashMap();
            //加载goods表商品数据
            TbGoods tbGoods = tbGoodsMapper.selectByPrimaryKey(goodsId);
            dateModel.put("goods", tbGoods);
            //加载goodDesc表的数据
            TbGoodsDesc tbGoodsDesc = tbGoodsDescMapper.selectByPrimaryKey(goodsId);
            dateModel.put("goodDesc", tbGoodsDesc);
            //面包屑导航,根据一级,二级,三级分类查询分类名称
            dateModel.put("Category1Name", tbItemCatMapper.selectByPrimaryKey(tbGoods.getCategory1Id()).getName());
            dateModel.put("Category2Name", tbItemCatMapper.selectByPrimaryKey(tbGoods.getCategory2Id()).getName());
            dateModel.put("Category3Name", tbItemCatMapper.selectByPrimaryKey(tbGoods.getCategory3Id()).getName());
            //读取商品sku表
            TbItemExample example = new TbItemExample();
            TbItemExample.Criteria criteria = example.createCriteria();
            criteria.andGoodsIdEqualTo(goodsId);
            criteria.andStatusEqualTo("2");//审核通过的
            example.setOrderByClause("is_default desc");//按照是否默认状态降序
            List<TbItem> items = tbItemMapper.selectByExample(example);
            dateModel.put("items", items);

            //创建write对象

            /*Writer out = new FileWriter(pagedir+goodsId+".html");*/
            FileWriterWithEncoding out = new FileWriterWithEncoding(pagedir + goodsId + ".html", "utf-8");//指定输出的字符集为utf-8格式


            //输出
            template.process(dateModel, out);
            //关流
            out.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 删除静态页面
     *
     * @param goodsIds
     * @return
     */
    @Override
    public Boolean deleItemHtml(List goodsIds) {


        try {
            for (Object goodsId : goodsIds) {
                new File(pagedir + (Long) goodsId + ".html").delete();
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }


    }


}




