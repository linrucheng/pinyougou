app.controller("itemController", function ($scope, $http) {

    //对商品数量增加
    $scope.addNum = function (x) {
        $scope.num = $scope.num + x;
        if ($scope.num < 1) {
            $scope.num = 1
        }
    }
    //记录用户选择的规格,对象定义规格格式:{"网络":"移动3G"},再判断
    $scope.specificationItems = {};
    $scope.selectSpecification = function (key, value) {
        $scope.specificationItems[key] = value;
        //在选中规格后再查看价格,标题,和id
        $scope.searchSku();

    }
    //判断规格有没有被选中
    $scope.isSelected = function (key, value) {
        if ($scope.specificationItems[key] == value) {
            return true;
        } else {
            return false;
        }
    }

    //默认加载sku列表
    $scope.loadItem = function () {
        $scope.sku = itemList[0];
        /*需要将spec的数据深克隆给specificationItems*/
        $scope.specificationItems = JSON.parse(JSON.stringify($scope.sku.spec))
    }
    //判断两个是否相等
    $scope.objectMatch = function (map1, mp2) {
        for (var k in map1) {
            if (map1[k] !== mp2[k]) {
                return false;
            }
        }
        //去除mp1,{"网络":"移动3G"}  mp2{"网络":"移动3G","手机内存":"32G"}的情况
        for (var k in mp2) {
            if (mp2[k] != map1[k]) {
                return false;
            }
        }
        return true;
    }
    //判断用户选择的  $scope.specificationItems = {};在sku列表中有没有
    $scope.searchSku = function () {
        for (var i = 0; i < itemList.length; i++) {
            //判断用户选择的对象再数组中有没有,如果有
            if ($scope.objectMatch($scope.specificationItems, itemList[i].spec)) {
                //用户选择的在列表中能查询到,将js对象赋值给 $scope.sku
                $scope.sku = itemList[i];
                return;

            }
        }
        $scope.sku = {
            "id": 0,
            "price": 0,
            "title": "------",
        }
    }
    //添加至购物车
    $scope.addItemCart = function () {

        //跨域请求
        $http.get('http://localhost:9107/cart/addItemToCartList.do?itemId=' +
            $scope.sku.id + '&num=' + $scope.num,{'withCredentials':true}).success(
                function (response) {
                    if(response.success){
                        location.href="http://localhost:9107/cart.html"
                    }else {
                        alert(response.message)
                    }


        })

    }


})