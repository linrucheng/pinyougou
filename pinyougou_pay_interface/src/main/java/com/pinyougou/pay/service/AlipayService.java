package com.pinyougou.pay.service;

import java.util.Map;

/**
 * 支付宝支付
 */
public interface AlipayService {

    /**
     * 生成支付宝二维码
     *
     * @param out_trade_no 支付订单号
     * @param total_fee    支付总金额
     * @return
     */
    public Map createNative(String out_trade_no, String total_fee);

    public Map queryPayStatus(String out_trade_no);
}
