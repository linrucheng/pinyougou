package com.pinyougou.pay.service;

import com.pinyougou.pojo.TbPayLog;

import java.util.Map;

public interface WeixinPayService {

    /**生成微信二维码
     * @param out_trade_no 支付订单号
     * @param total_fee    金额(分)
     * @return
     */
    public Map cerateNative(String out_trade_no, String total_fee);

    /**
     * @param out_trade_no 商户订单号
     * @return 询问订单状态
     */
    public Map queryPayStatus(String out_trade_no);

    /**
     * 关闭微信订单
     * @param out_trade_no
     * @return
     */
    public Map closePay(String out_trade_no);





}
