package com.pinyougou.pay.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.wxpay.sdk.WXPayUtil;
import com.pinyougou.pay.service.WeixinPayService;
import com.pinyougou.pojo.TbPayLog;
import com.pinyougou.utils.HttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: pinyougou_parent
 * @name: WeixinPayServiceImpl
 * @author;lin
 * @create: 2019-07-22 22:11
 **/
@Service
public class WeixinPayServiceImpl implements WeixinPayService {
    @Value("${wx_appid}")
    private String appid;


    @Value("${partner}")
    private String partner;

    @Value("${partnerkey}")
    private String partnerkey;

    @Value("${notifyurl}")
    private String notifyurl;

    @Value("${CREATE_NATIVE_PAY_URL}")
    private String CREATE_NATIVE_PAY_URL;

    @Value("${QUERY_ORDER_PAY_STATUS}")
    private String QUERY_ORDER_PAY_STATUS;

    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * @param out_trade_no 订单号
     * @param total_fee    金额(分)
     * @return 生成二维码
     */
    @Override
    public Map cerateNative(String out_trade_no, String total_fee) {
        //构建参数

        Map<String, String> param = new HashMap<>();//请求参数
        param.put("appid", appid);//公众账号ID
        param.put("mch_id", partner);//商户号
        param.put("nonce_str", WXPayUtil.generateNonceStr());//随机字符串
        param.put("body", "品优购");//商品描述
        param.put("out_trade_no", out_trade_no);//商户订单号
        param.put("total_fee", total_fee);//标价金额
        param.put("spbill_create_ip", "127.0.0.1");//终端IP
        param.put("notify_url", "http://test.itcast.cn");//回调地址,随便填
        param.put("trade_type", "NATIVE");//交易类型

        try {
            //MAP 转换为 XML 字符串（自动添加签名）
            String xml = WXPayUtil.generateSignedXml(param, partnerkey);
            HttpClient client = new HttpClient("https://api.mch.weixin.qq.com/pay/unifiedorder");
            client.setHttps(true);//是否是 https 协议
            client.setXmlParam(xml);//发送的 xml 数据
            client.post();//执行 post 请求
            String resultXml = client.getContent(); //获取结果
            //将xml转为map
            Map<String, String> map = WXPayUtil.xmlToMap(resultXml);
            Map reslutMap = new HashMap();
            reslutMap.put("code_url", map.get("code_url"));//二维码链接
            reslutMap.put("total_fee", total_fee);//总金额
            reslutMap.put("out_trade_no", out_trade_no);//订单号
            return reslutMap;
        } catch (Exception e) {
            e.printStackTrace();
            return new HashMap();
        }
    }

    /**
     * @param out_trade_no 商户订单号
     * @return
     */
    @Override
    public Map queryPayStatus(String out_trade_no) {

        Map<String, String> param = new HashMap<>();//请求参数
        param.put("appid", appid);//公众账号ID
        param.put("mch_id", partner);//商户号
        param.put("nonce_str", WXPayUtil.generateNonceStr());//随机字符串

        param.put("out_trade_no", out_trade_no);//商户订单号
        try {
            String xml = WXPayUtil.generateSignedXml(param, partnerkey);
            HttpClient client = new HttpClient("https://api.mch.weixin.qq.com/pay/orderquery");
            client.setHttps(true);//是否是 https 协议
            client.setXmlParam(xml);//发送的 xml 数据
            client.post();//执行 post 请求
            String resultXml = client.getContent(); //获取结果
            Map<String, String> map = WXPayUtil.xmlToMap(resultXml);
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 关闭微信订单
     *
     * @param out_trade_no
     * @return
     */
    @Override
    public Map closePay(String out_trade_no) {
        Map param = new HashMap();
        param.put("appid", appid);//公众账号 ID
        param.put("mch_id", partner);//商户号
        param.put("out_trade_no", out_trade_no);//订单号
        param.put("nonce_str", WXPayUtil.generateNonceStr());//随机字符串
        String url = "https://api.mch.weixin.qq.com/pay/closeorder";
        try {
            String xmlParam = WXPayUtil.generateSignedXml(param, partnerkey);
            HttpClient client = new HttpClient(url);
            client.setHttps(true);
            client.setXmlParam(xmlParam);
            client.post();
            String result = client.getContent();
            Map<String, String> map = WXPayUtil.xmlToMap(result);
            System.out.println(map);
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}















