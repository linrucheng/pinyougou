package com.pinyougou.entity;

import java.io.Serializable;
import java.util.List;

/**
 * @program: pinyougou_parent
 * @name: PageResult
 * @author;lin
 * @create: 2019-06-29 15:29
 * 查询到的分页数据
 **/
public class PageResult<T> implements Serializable {
    private Long total;//总页数
    private List<T> rows;//查询到的记录数

    public PageResult() {
    }

    public PageResult(Long total, List<T> rows) {
        this.total = total;
        this.rows = rows;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }
}




