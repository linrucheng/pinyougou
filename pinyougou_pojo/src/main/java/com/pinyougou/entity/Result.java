package com.pinyougou.entity;

import java.io.Serializable;

/**
 * @program: pinyougou_parent
 * @name: Result
 * @author;lin
 * @create: 2019-06-29 20:09
 * 对增删改的返回给页面的结果封装
 **/
public class Result implements Serializable {
    private boolean success;//操作是否成功
    private String message;//返回给页面的提示信息

    public Result(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public Result() {
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}




