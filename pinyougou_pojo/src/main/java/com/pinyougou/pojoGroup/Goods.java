package com.pinyougou.pojoGroup;

import com.pinyougou.pojo.TbGoods;
import com.pinyougou.pojo.TbGoodsDesc;
import com.pinyougou.pojo.TbItem;

import java.io.Serializable;
import java.util.List;

/**
 * @program: pinyougou_parent
 * @name: Goods
 * @author;lin
 * @create: 2019-07-03 21:17
 * <p>
 * 商品的组合实体类
 **/
public class Goods implements Serializable {
    private TbGoods tbGoods;//spu
    private TbGoodsDesc tbGoodsDesc;//spu的其他描述
    private List<TbItem> tbItems;//sku spu与sku是一对多关系

    public TbGoods getTbGoods() {
        return tbGoods;
    }

    public void setTbGoods(TbGoods tbGoods) {
        this.tbGoods = tbGoods;
    }

    public TbGoodsDesc getTbGoodsDesc() {
        return tbGoodsDesc;
    }

    public void setTbGoodsDesc(TbGoodsDesc tbGoodsDesc) {
        this.tbGoodsDesc = tbGoodsDesc;
    }

    public List<TbItem> getTbItems() {
        return tbItems;
    }

    public void setTbItems(List<TbItem> tbItems) {
        this.tbItems = tbItems;
    }
}








