app.controller('baseController', function ($scope) {
    $scope.paginationConf = {
        currentPage: 1,  //当前页 //分页插件加载会自动触发onChange事件
        totalItems: 10,//总记录数
        itemsPerPage: 10,//每页记录数
        perPageOptions: [10, 20, 30, 40, 50], //分页选项
        onChange: function () {//生每当上面的数据发改变时,会执行onChange的函数
            $scope.reloadList();
        }
    }
    $scope.reloadList = function () {
        $scope.search($scope.paginationConf.currentPage, $scope.paginationConf.itemsPerPage);
        // $scope.findByPage($scope.paginationConf.currentPage, $scope.paginationConf.itemsPerPage);
    }
    //获取复选框的id数组
    $scope.selectIds = [];
    $scope.updateSelectId = function ($event, id) {
        if ($event.target.checked) {
            //如果选中的话,将id加上去
            $scope.selectIds.push(id);
        } else {
            //获取将id数组中移除
            //获取id的索引
            var indexId = $scope.selectIds.indexOf(id);
            $scope.selectIds.splice(indexId, 1);
        }
    }
    
    
    //将字符串转化为json
    
    $scope.stringToJson=function (string,key) {
        var json=JSON.parse(string)
        var value="";
        for (var i=0;i<json.length;i++){
            if (i>0){
                value=value+",";
            }
            value=value+json[i][key];
        }
        return value
    }






})