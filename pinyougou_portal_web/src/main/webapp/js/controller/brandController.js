app.controller("brandController", function ($scope,  brandService, $controller) {//定义控制器
    //没有和后端交互的方法抽取到符控制器中
    $controller('baseController', {$scope:$scope})//继承baseController控制器

    //分
    $scope.findByPage = function (page, size) {
        brandService.findByPage.success(
            function (response) {
                $scope.list = response.list;
                $scope.paginationConf.totalItems = response.totalPage;
            })
    }
    //保存

    $scope.save = function () {
        /*判断要放在方法里面,进行判断时候再调用,如果没有放在方法里面,页面加载时候回提示变量未定义*/
        var object = null;
        if ($scope.entity.id != null) {
            //更新
            object = brandService.update($scope.entity);
        } else {
            //保存
            object = brandService.add($scope.entity);
        }
        object.success(
            function (response) {
                if (response.success) {
                    $scope.reloadList()
                } else {
                    alert(response.message)
                }
            })
    }


    //查询一个
    $scope.findOne = function (id) {
        brandService.findOne(id).success(function (response) {
            $scope.entity = response;
        })
    }

    //删除
    $scope.del = function () {
        brandService.del($scope.selectIds).success(function (response) {
            if (response.success) {
                $scope.reloadList();
            } else {
                alert(response.message);
            }
        })
    }
    //分页+查询
    $scope.entitySearch = {};
    $scope.search = function (page, size) {
        brandService.search(page, size, $scope.entitySearch).success(
            function (response) {
                $scope.list = response.rows;
                $scope.paginationConf.totalItems = response.total;
            }
        )
    }


})