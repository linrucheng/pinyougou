//控制层
app.controller('goodsController', function ($scope, $controller, $location, goodsService, itemCatService, uploadService, typeTemplateService) {

    $controller('baseController', {$scope: $scope});//继承

    //读取列表数据绑定到表单中  
    $scope.findAll = function () {
        goodsService.findAll().success(
            function (response) {
                $scope.list = response;
            }
        );
    }

    //分页
    $scope.findPage = function (page, rows) {
        goodsService.findPage(page, rows).success(
            function (response) {
                $scope.list = response.rows;
                $scope.paginationConf.totalItems = response.total;//更新总记录数
            }
        );
    }

    //查询实体
    $scope.findOne = function () {
        //获取页面传递过来的id
        var id = $location.search()['id']
        $scope.id = id;

        if (id == null) {
            return;
        }
        goodsService.findOne(id).success(
            function (response) {
                $scope.entity = response;
                editor.html($scope.entity.tbGoodsDesc.introduction)//商品介绍 富文本
                $scope.entity.tbGoodsDesc.itemImages = JSON.parse($scope.entity.tbGoodsDesc.itemImages)//照片
                //获取规格选项,字符串需要转化为json
                $scope.entity.tbGoodsDesc.customAttributeItems = JSON.parse($scope.entity.tbGoodsDesc.customAttributeItems)//扩展属性
                $scope.entity.tbGoodsDesc.specificationItems = JSON.parse($scope.entity.tbGoodsDesc.specificationItems)//规格
                //sku

                for (var i = 0; i < $scope.entity.tbItems.length; i++) {
                    $scope.entity.tbItems[i].spec = JSON.parse($scope.entity.tbItems[i].spec)
                }
            }
        );
    }


    //保存
    $scope.save = function () {
        var serviceObject;//服务层对象
        if ($location.search()['id'] != null) {//如果有ID
            serviceObject = goodsService.update($scope.entity); //修改
        } else {
            serviceObject = goodsService.add($scope.entity);//增加
        }
        serviceObject.success(
            function (response) {
                if (response.success) {
                    //重新查询
                    alert("保存成功")


                    if ($location.search()['id'] != null) {
                        //修改后跳转到列表页面
                        location.href = "goods.html"
                    } else {
                        //增加页面后页面清空
                        $scope.entity = {};
                        editor.html('')
                        $scope.reloadList();//重新加载
                    }

                } else {
                    alert(response.message);
                }
            }
        ).error(function () {
            alert("发生未知错误")
        });
        ;
    }

    /*
        //保存
        $scope.save = function () {
            //提取富文本编辑器的内容
            $scope.entity.tbGoodsDesc.introduction = editor.html();



            goodsService.add($scope.entity).success(function (response) {
                    if (response.success) {
                        //重新查询
                        alert("新增成功")
                        //页面信息清空
                        $scope.entity = {};
                        editor.html('');//清空富文本编辑器的内容
                    } else {
                        alert(response.message);
                    }
                }
            ).error(function () {
                alert("发生未知错误")
            });
        }
    */


    //批量删除
    $scope.dele = function () {
        //获取选中的复选框
        goodsService.dele($scope.selectIds).success(
            function (response) {
                if (response.success) {
                    $scope.reloadList();//刷新列表
                    $scope.selectIds = [];
                }
            }
        );
    }

    $scope.searchEntity = {};//定义搜索对象

    //搜索
    $scope.search = function (page, rows) {
        goodsService.search(page, rows, $scope.searchEntity).success(
            function (response) {
                $scope.list = response.rows;
                $scope.paginationConf.totalItems = response.total;//更新总记录数
            }
        );
    }	//上传图片


    //文件上传
    $scope.upload = function () {
        uploadService.uploadFile().success(function (response) {
            if (response.success) {
                //文件上传成功,去除url
                $scope.image_entity.url = response.message;//去除文件地址
            } else {
                alert(response.message)
            }
        }).error(function () {
            alert("发生未知错误")//当后台发生500会弹出此提示框
        })
    }

    //添加图片列表
    $scope.entity = {tbGoods: {isEnableSpec: '0'}, tbGoodsDesc: {itemImages: [], specificationItems: []}};
    $scope.add_img_entity = function () {
        $scope.entity.tbGoodsDesc.itemImages.push($scope.image_entity)

    }

    //删除图片
    $scope.dele_img_entity = function ($event) {

        $scope.entity.tbGoodsDesc.itemImages.splice($event, 1)
    }


    //商品分类一级列表
    $scope.findItemCatList1 = function () {
        itemCatService.findByParentId('0').success(function (response) {
            $scope.item1CatList = response;

        })
    }
    //二级商品分类
    //监听商品一级分类的id entity.tbGoods.category1Id 若发生改变,则触发函数

    $scope.$watch('entity.tbGoods.category1Id', function (newValue, oldValue) {
        if (newValue != undefined) {
            itemCatService.findByParentId(newValue).success(function (response) {
                $scope.item2CatList = response;
                $scope.item3CatList = [];//保证三级类目为空
            })
        }

    })
    //三级商品分类
    $scope.$watch('entity.tbGoods.category2Id', function (newValue, oldValue) {
        if (newValue != undefined) {
            itemCatService.findByParentId(newValue).success(function (response) {
                $scope.item3CatList = response;
                $scope.entity.tbGoods.typeTemplateId = undefined;//保证模板Id为空

            })
        }
    })

    //模板id监听三级目录的id,得到模板Id
    $scope.$watch('entity.tbGoods.category3Id', function (newValue, oldValue) {
        if (newValue != undefined) {
            itemCatService.findOne(newValue).success(function (response) {
                $scope.entity.tbGoods.typeTemplateId = response.typeId;
            })
        }
    })

    //品牌,监听模板id,
    $scope.entity.tbGoodsDesc.customAttributeItems = [];
    $scope.$watch('entity.tbGoods.typeTemplateId', function (newValue, oldValue) {
        if (newValue != undefined) {
            typeTemplateService.findOne(newValue).success(function (response) {
                $scope.typeTemplate = response;//查到的是一个模板的typeTemplate

                $scope.typeTemplate.brandIds = JSON.parse($scope.typeTemplate.brandIds);//品牌转化为json

                //在查询一个进行内容会显时,模板id从undifined到有值,它会从模板里面取,
                //由于模板的typeTemplate.customAttributeItems为[{"text":"内存大小"},{"text":"颜色"}],会导致又查询一次进行回显,没有value值
                //从entity.tbGoodsDesc.customAttributeItems查询的数据[{"text":"内存大小","value":"10M"},{"text":"颜色","value":"花"}]
                //进行判断

                if ($location.search()['id'] == null) {
                    //没有向页面传参数,即进行新增操作
                    $scope.entity.tbGoodsDesc.customAttributeItems = JSON.parse($scope.typeTemplate.customAttributeItems)//扩展属性

                }


            })
            typeTemplateService.findTypeTemplateList(newValue).success(function (response) {
                $scope.specificationList = response;//[{"id":27,"text":"网络","option":[]},{"id":32,"text":"机身内存","option":[]}]
            })
        }
    })

    //勾选样式的复选框
    $scope.updateSpecAttrbute = function ($event, name, value) {
        //name为attributeName的值 网络制式  value为attributeValue的值
        //规格选项tb_goodDesc specification_items
        //[{"attributeName":"网络制式","attributeValue":["移动3G","移动4G"]},{"attributeName":"屏幕尺寸","attributeValue":["6寸","5寸"]}]
        //判断name在集合中存不存在
        var object = $scope.findObjectByKey($scope.entity.tbGoodsDesc.specificationItems, "attributeName", name);
        if (object != null) {
            //说明存在,向"attributeValue":["移动3G","移动4G"]这个数组中添加值
            if ($event.target.checked) {
                //假如是选中状态,向集合中添加
                object.attributeValue.push(value)
            } else {
                //假如没有选中,向这个"attributeValue":["移动3G","移动4G"]集合中移除
                object.attributeValue.splice(object.attributeValue.indexOf(value), 1)

                //判断object.attributeValue的数组为[],则大数组移除object对象{"attributeName":"网络制式","attributeValue":[]} object 这一行
                if (object.attributeValue.length == 0) {
                    //[{"attributeName":"网络制式","attributeValue":[]},{"attributeName":"屏幕尺寸","attributeValue":["6寸","5寸"]}]
                    $scope.entity.tbGoodsDesc.specificationItems.splice($scope.entity.tbGoodsDesc.specificationItems.indexOf(object), 1)
                }
            }
        } else {
            //如果不存在,向集合中添加一个对象{"attributeName":"网络制式","attributeValue":["移动3G"]}
            $scope.entity.tbGoodsDesc.specificationItems.push({'attributeName': name, 'attributeValue': [value]})
        }
    }
    //创建sku表
    $scope.createItemList = function () {
        $scope.entity.tbItems = [{spec: {}, price: 0, num: 0, status: 0, isDefault: 0}];//初始化

        //[{"attributeName":"网络制式","attributeValue":["移动3G","移动4G"]},{"attributeName":"屏幕尺寸","attributeValue":["6寸","5寸"]}]
        var items = $scope.entity.tbGoodsDesc.specificationItems
        //遍历items
        for (var i = 0; i < items.length; i++) {
            // 向这个集合$scope.entity.item = [{spec: {}, price: 0, num: 0, status: 0, isDefault: 0}]添加
            //columnName为attributeName,columnValues为["移动3G","移动4G"]的元素
            $scope.entity.tbItems = addColumn($scope.entity.tbItems, items[i].attributeName, items[i].attributeValue);
        }
    }


    //创建方法向集合中添加一个列和值
    //list,集合,columnName列的名称,columnValues列的值[]数组形式
    addColumn = function (list, columnName, columnValues) {
        var newList = [];
        for (var i = 0; i < list.length; i++) {
            var oldRow = list[i]//{spec:[],price:0,num:0,status:0,isDefault:0}
            //遍历columnValues集合
            for (var j = 0; j < columnValues.length; j++) {
                //克隆oldRowcolumnValues[联通3G,联通4G]
                var newRow = JSON.parse(JSON.stringify(oldRow));
                newRow.spec[columnName] = columnValues[j];//第一次循环 {spec:{网络:[联通3G],price:0,num:0,status:0,isDefault:0}}
                newList.push(newRow)//newList[{spec:{网络:[联通3G]},price:0,num:0,status:0,isDefault:0},]
            }
        }

        return newList;
    }

    $scope.status = ["未审核", "申请中", "审核通过", "已关闭"]
    $scope.itemCatList = [];

    $scope.findAll = function () {
        itemCatService.findAll().success(
            function (response) {
                for (var i = 0; i < response.length; i++) {
                    $scope.itemCatList[response[i].id] = response[i].name;
                }
            })

    }
    //判断规格复选框有没有选中
    //[{"attributeName":"网络制式","attributeValue":["移动3G","移动4G"]},{"attributeName":"屏幕尺寸","attributeValue":["6寸","5寸"]}]
    $scope.specChecked = function (name, optionName) {
        var specs = $scope.entity.tbGoodsDesc.specificationItems
        //判断指定建和值在数组中有没有
        var object = $scope.findObjectByKey(specs, 'attributeName', name);
        //判断

        if (object == null) {
            //即在集合中没有,返回false不显示
            return false;
        } else {
            //在集合中有,判断"attributeValue":["移动3G","移动4G"]的属性有没有
            //判断"移动3G"在集合中的索引有没有
            if (object.attributeValue.indexOf(optionName) >= 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    //提交审核

    $scope.updateStatus = function (status) {
        //获取选中的复选框,$scope.list 获取所有的数据

        goodsService.updateStatus($scope.selectIds, status).success(
            function (response) {
                if (response.success) {
                    alert(response.message)
                    $scope.reloadList();//刷新列表
                    $scope.selectIds = [];
                }
            }
        );
    }








})







