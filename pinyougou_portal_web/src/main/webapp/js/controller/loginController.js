//控制层
app.controller('loginController', function ($scope, $controller, loginService) {
    $controller('baseController', {$scope: $scope});//继承

    $scope.getUsername = function () {
        loginService.login().success(function (response) {
            $scope.entity = response;
        })
    }
})
