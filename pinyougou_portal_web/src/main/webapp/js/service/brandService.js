app.service("brandService", function ($http) {
    //分页
    this.findByPage = function (page, size) {
        return $http.get("../brand/findByPage.do?currentPage=" + page + "&pageSize=" + size)
    }
    //查询一个
    this.findOne = function (id) {
        return $http.get("../brand/findOne.do?id=" + id)
    }
    //添加
    this.add = function (entity) {
        return $http.post("../brand/add.do", entity)
    }
    //修改
    this.update = function (entity) {
        return $http.post("../brand/update.do", entity)
    }
    //删除
    this.del = function (ids) {
        return $http.get("../brand/delete.do?ids=" + ids)
    }
    //查询+分页
    this.search = function (page, size, entitySearch) {
        return $http.post("../brand/findByPageAndCondition.do?currentPage=" + page + "&pageSize=" + size, entitySearch)
    }
    this.selectOptionList = function () {
        return $http.get("../brand/selectBrandList.do")
    }

})