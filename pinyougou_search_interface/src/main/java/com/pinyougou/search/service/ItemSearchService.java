package com.pinyougou.search.service;

import com.pinyougou.pojo.TbItem;

import java.util.List;
import java.util.Map;

public interface ItemSearchService {

    /**
     * 搜索
     *
     * @param searchMap
     * @return
     */
    public Map<String, Object> search(Map<String, Object> searchMap);

    public void saveItemList(List<TbItem> list);


    public  void deleteByGoodIds(List ids);
}
