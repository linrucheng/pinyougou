package com.pinyougou.search.service.impl;

import com.alibaba.fastjson.JSON;
import com.pinyougou.pojo.TbItem;
import com.pinyougou.search.service.ItemSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.util.List;

/**
 * @program: pinyougou_parent
 * @name: ItemSearchListener
 * @author;lin
 * @create: 2019-07-16 14:12
 * 监听点对点消息
 **/
@Component
public class ItemSearchDeleteListener implements MessageListener {
    @Autowired
    private ItemSearchService itemSearchService;

    /**
     * 监听消息
     *
     * @param message
     */
    @Override
    public void onMessage(Message message) {
        //接收消息,强转为TextMessage--一个字符串对象
        TextMessage textMessage = (TextMessage) message;
        try {
            String itemListStr = textMessage.getText();
            System.out.println("接收到消息" + itemListStr);
            //将字符串转为list集合
            List list = JSON.parseArray(itemListStr);
            //shan

            itemSearchService.deleteByGoodIds(list);


        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}









