package com.pinyougou.search.service.impl;

import com.alibaba.dubbo.config.annotation.Service;

import com.pinyougou.pojo.TbItem;
import com.pinyougou.search.service.ItemSearchService;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.*;
import org.springframework.data.solr.core.query.result.*;

import java.util.*;

/**
 * @program: pinyougou_parent
 * @name: ItemSearchServiceImpl
 * @author;lin
 * @create: 2019-07-10 19:35
 **/
@Service(timeout = 3000)//防止sevice执行时间过长,dubbox报超时,默认1s
public class ItemSearchServiceImpl implements ItemSearchService {

    @Autowired
    private SolrTemplate solrTemplate;

    @Override
    public Map<String, Object> search(Map<String, Object> searchMap) {
        /*
         * 从缓存中将分类,品牌和规格取出来,发给前台
         * 前台发送查询条件,后台进行查询
         *
         * */


        if ("".equals(searchMap.get("keywords")) || searchMap.get("keywords") == null) {
            Map map = new HashedMap();

            return map;
        }
        String keywords = (String) searchMap.get("keywords");
        //将空格剔除
        searchMap.put("keywords", keywords.replace(" ", ""));


        //高亮显示
        Map map = new HashedMap();
        Map<String, Object> serachList = serachList(searchMap);//查询到的内容
        map.putAll(serachList);


        //查询分类
        List list = searchCategoryList(searchMap);
        map.put("cateGoryList", list);

        //查询品牌和规格
        if (list != null && list.size() > 0) {
            Map brandListAndSpecList = null;
            //判断是否有分类名称
            if ("".equals(searchMap.get("category"))) {
                //假设没有分类名称,取第一个
                brandListAndSpecList = findBrandListAndSpecList((String) list.get(0));
            } else {
                //假如有分类名称,则取分类名称,
                brandListAndSpecList = findBrandListAndSpecList((String) searchMap.get("category"));
            }
            map.putAll(brandListAndSpecList);


        }
        return map;
    }


    private Map<String, Object> serachList(Map<String, Object> searchMap) {
        Map map = new HashMap();
        //对返回结果的关键字进行高亮显示
        // Query query = new SimpleQuery();
        HighlightQuery query = new SimpleHighlightQuery();

        HighlightOptions options = new HighlightOptions().addField("item_title");//设置高亮的域
        options.setSimplePrefix("<em style='color:red'>");  //设置高亮前缀
        options.setSimplePostfix("</em>");//设置高亮后缀
        query.setHighlightOptions(options);//设置值


        //创建查询条件
        //指明动态域名,和关键字 is表示会将条件进行分词处理
        Criteria criteria = new Criteria("item_keywords").is(searchMap.get("keywords"));
        query.addCriteria(criteria);


        //1.1分类筛选
        if (!"".equals(searchMap.get("category"))) {
            Criteria filterCriteria = new Criteria("item_category").is(searchMap.get("category"));
            FilterQuery filterQuery = new SimpleFilterQuery(filterCriteria);
            query.addFilterQuery(filterQuery);
        }
        //1.2品牌筛选
        if (!"".equals(searchMap.get("brand"))) {
            Criteria filterCriteria = new Criteria("item_brand").is(searchMap.get("brand"));
            FilterQuery filterQuery = new SimpleFilterQuery(filterCriteria);
            query.addFilterQuery(filterQuery);
        }
        //1.3规格筛选 spec: {网络: "移动3G", 机身内存: "16G"} 是这种形式,需要转化为map格式,循环遍历
        if (searchMap.get("spec") != null) {
            Map<String, String> specMap = (Map<String, String>) searchMap.get("spec");
            Set<String> keySet = searchMap.keySet();
            for (String key : keySet) {
                String value = specMap.get(key);
                Criteria filterCriteria = new Criteria("item_spec_" + key).is(value);
                FilterQuery filterQuery = new SimpleFilterQuery(filterCriteria);
                query.addFilterQuery(filterQuery);
            }
        }
        //1.4价格筛选
        if (!"".equals(searchMap.get("price"))) {
            //获取价格
            String price = (String) searchMap.get("price");
            String[] strArray = price.split("-");
            //0-500 设置条件 小于500
            //500-1000
            //5000-*

            //strArray[0]-strArray[1]
            Criteria filterCriteria1 = new Criteria("item_price").greaterThan(strArray[0]);
            FilterQuery filterQuery1 = new SimpleFilterQuery(filterCriteria1);
            query.addFilterQuery(filterQuery1);
            //如果第二个不为*,则添加上限
            if (!"*".equals(strArray[1])) {
                //如果最后一个字符串不为*
                Criteria filterCriteria = new Criteria("item_price").lessThan(strArray[1]);
                FilterQuery filterQuery = new SimpleFilterQuery(filterCriteria);
                query.addFilterQuery(filterQuery);
            }
        }
        //1.5分页在添加条件后增加

        Integer currentPage = (Integer) searchMap.get("currentPage");
        ;//当前页
        if (currentPage == null) {
            currentPage = 1;//默认第一页
        }
        Integer pageSize = (Integer) searchMap.get("pageSize"); //Integer.parseInt((String) searchMap.get("pageSize"));//每页记录数
        if (pageSize == null) {
            pageSize = 20;//默认二十条记录
        }
        query.setOffset((currentPage - 1) * pageSize);//设置起始页
        query.setRows(pageSize);//设置每页记录数
        //1.6价格排序
        String sortValue = (String) searchMap.get("sort");//排序方式,ASC升序,DESC降序
        String sortField = (String) searchMap.get("sortField");//排序字段
        if (sortField != null && !"".equals(sortField)) {
            if ("ASC".equals(sortValue)) {
                //升序
                Sort sort = new Sort(Sort.Direction.ASC, "item_" + sortField);
                query.addSort(sort);
            }
            if ("DESC".equals(sortValue)) {
                //降序
                Sort sort = new Sort(Sort.Direction.DESC, "item_" + sortField);
                query.addSort(sort);
            }
        }
        //1.7新品排序

    /*    String timeValue = (String) searchMap.get("time");//排序方式,DESC降序
        if ("DESC".equals(timeValue)) {
            Sort sort = new Sort(Sort.Direction.DESC, "item_updateTime");
            query.addSort(sort);
        }*/


        //得到高亮页
        HighlightPage<TbItem> tbItems = solrTemplate.queryForHighlightPage(query, TbItem.class);
        //List<HighlightEntry<TbItem>> highlighted = tbItems.getHighlighted();


        int totalPages = tbItems.getTotalPages();//得到总页数
        long totalElements = tbItems.getTotalElements();//得到记录数
        map.put("totalPages", totalPages);
        map.put("totalElements", totalElements);


        //循环高亮入口集合
        for (HighlightEntry<TbItem> h : tbItems.getHighlighted()) {
            TbItem item = h.getEntity();//获取原实体类
            if (h.getHighlights().size() > 0 && h.getHighlights().get(0).getSnipplets().size() > 0) {
                item.setTitle(h.getHighlights().get(0).getSnipplets().get(0));//设置高亮结果
            }
        }
        map.put("rows", tbItems.getContent());//查询到的内容
        return map;
    }

    /**
     * 查询分类列表
     *
     * @param searchMap
     * @return
     */
    private List searchCategoryList(Map searchMap) {
        List list = new ArrayList();
        Query query = new SimpleQuery();
        //根据关键字查询
        Criteria criteria = new Criteria("item_keywords").is(searchMap.get("keywords"));
        query.addCriteria(criteria);
        //按照分类来分组
        GroupOptions groupOptions = new GroupOptions().addGroupByField("item_category");
        query.setGroupOptions(groupOptions);
        //得到分组页
        GroupPage<TbItem> tbItems = solrTemplate.queryForGroupPage(query, TbItem.class);
        //得到分组结果集
        GroupResult<TbItem> itemCategory = tbItems.getGroupResult("item_category");
        //得到分组入口页
        Page<GroupEntry<TbItem>> entries = itemCategory.getGroupEntries();
        //得到分组入口页集合
        List<GroupEntry<TbItem>> content = entries.getContent();
        for (GroupEntry<TbItem> groupEntry : content) {
            String value = groupEntry.getGroupValue();//得到分组结果的名称
            list.add(value);
        }
        return list;
    }

    /**
     * 根据类名,取品牌和规格
     *
     * @param name
     * @return
     */
    @Autowired
    private RedisTemplate redisTemplate;

    private Map findBrandListAndSpecList(String name) {
        //跟句类名从缓存中取模板id
        Map map = new HashedMap();
        Long id = (Long) redisTemplate.boundHashOps("itemCatList").get(name);

        //根据模板id从缓存中取品牌
        List<Map> brandList = (List<Map>) redisTemplate.boundHashOps("brandList").get(id);
        map.put("brandList", brandList);
        //根据模板id从缓存中取规格
        List<Map> specList = (List<Map>) redisTemplate.boundHashOps("specList").get(id);
        map.put("specList", specList);
        return map;
    }


    /**
     * 将集合保存在索引库
     *
     * @param list
     */
    public void saveItemList(List<TbItem> list) {
        solrTemplate.saveBeans(list);
        solrTemplate.commit();
    }

    /**
     * 删除索引库
     *
     * @param ids
     */
    @Override
    public void deleteByGoodIds(List ids) {
        System.out.println("开始删除了");
        Query query = new SimpleQuery();
        //根据域名称和id的集合删除
        Criteria criteria = new Criteria("item_goodsid").is(ids);
        query.addCriteria(criteria);
        solrTemplate.delete(query);
        solrTemplate.commit();
    }


}






