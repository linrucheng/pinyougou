package com.pinyougou.seckill.service;

import com.pinyougou.pojo.TbSeckillOrder;

public interface SeckillOrderService {

    /**
     * 从缓存中查询订单
     * @param userId
     * @return
     */
    public TbSeckillOrder searchOrderFromRedis(String userId);

    /**
     * 从缓存中读取订单
     * @param userId
     * @param orderId
     * @param transactionId
     */
    public void saveOrderFromRedisToDb(String userId,Long orderId,String transactionId);

    /**
     * 客户未付款,从缓存中删除该订单,恢复库存数量
     * @param userId
     * @param orderId
     */
    public void deleteOrderFromRedis(String userId,Long orderId);
}
