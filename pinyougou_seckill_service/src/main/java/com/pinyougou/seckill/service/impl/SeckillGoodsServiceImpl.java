package com.pinyougou.seckill.service.impl;

import java.util.Date;
import java.util.List;

import com.pinyougou.pojo.TbSeckillOrder;
import com.pinyougou.utils.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.pinyougou.mapper.TbSeckillGoodsMapper;
import com.pinyougou.pojo.TbSeckillGoods;
import com.pinyougou.pojo.TbSeckillGoodsExample;
import com.pinyougou.pojo.TbSeckillGoodsExample.Criteria;
import com.pinyougou.seckill.service.SeckillGoodsService;

import com.pinyougou.entity.PageResult;
import org.springframework.data.redis.core.RedisTemplate;


/**
 * 服务实现层
 *
 * @author Administrator
 */
@Service
public class SeckillGoodsServiceImpl implements SeckillGoodsService {

    @Autowired
    private TbSeckillGoodsMapper seckillGoodsMapper;

    /**
     * 查询全部
     */
    @Override
    public List<TbSeckillGoods> findAll() {
        return seckillGoodsMapper.selectByExample(null);
    }

    /**
     * 按分页查询
     */
    @Override
    public PageResult findPage(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        Page<TbSeckillGoods> page = (Page<TbSeckillGoods>) seckillGoodsMapper.selectByExample(null);
        return new PageResult(page.getTotal(), page.getResult());
    }

    /**
     * 增加
     */
    @Override
    public void add(TbSeckillGoods seckillGoods) {
        seckillGoodsMapper.insert(seckillGoods);
    }


    /**
     * 修改
     */
    @Override
    public void update(TbSeckillGoods seckillGoods) {
        seckillGoodsMapper.updateByPrimaryKey(seckillGoods);
    }

    /**
     * 根据ID获取实体
     *
     * @param id
     * @return
     */
    @Override
    public TbSeckillGoods findOne(Long id) {
        return seckillGoodsMapper.selectByPrimaryKey(id);
    }

    /**
     * 批量删除
     */
    @Override
    public void delete(Long[] ids) {
        for (Long id : ids) {
            seckillGoodsMapper.deleteByPrimaryKey(id);
        }
    }


    @Override
    public PageResult findPage(TbSeckillGoods seckillGoods, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);

        TbSeckillGoodsExample example = new TbSeckillGoodsExample();
        Criteria criteria = example.createCriteria();

        if (seckillGoods != null) {
            if (seckillGoods.getTitle() != null && seckillGoods.getTitle().length() > 0) {
                criteria.andTitleLike("%" + seckillGoods.getTitle() + "%");
            }
            if (seckillGoods.getSmallPic() != null && seckillGoods.getSmallPic().length() > 0) {
                criteria.andSmallPicLike("%" + seckillGoods.getSmallPic() + "%");
            }
            if (seckillGoods.getSellerId() != null && seckillGoods.getSellerId().length() > 0) {
                criteria.andSellerIdLike("%" + seckillGoods.getSellerId() + "%");
            }
            if (seckillGoods.getStatus() != null && seckillGoods.getStatus().length() > 0) {
                criteria.andStatusLike("%" + seckillGoods.getStatus() + "%");
            }
            if (seckillGoods.getIntroduction() != null && seckillGoods.getIntroduction().length() > 0) {
                criteria.andIntroductionLike("%" + seckillGoods.getIntroduction() + "%");
            }

        }

        Page<TbSeckillGoods> page = (Page<TbSeckillGoods>) seckillGoodsMapper.selectByExample(example);
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 查询所有的秒杀商品
     *
     * @return
     */
    @Override
    public List<TbSeckillGoods> findList() {

        List<TbSeckillGoods> tbSeckillGoods = redisTemplate.boundHashOps("seckillGoods").values();
        //如果缓存没有,则从数据库查询
        if (tbSeckillGoods == null || tbSeckillGoods.size() <= 0) {
            TbSeckillGoodsExample example = new TbSeckillGoodsExample();
            Criteria criteria = example.createCriteria();
            criteria.andStatusEqualTo("2");//审核通过
            //开始时间=<当前时间<=结束时间
            Date now = new Date();
            criteria.andStartTimeLessThanOrEqualTo(now);
            criteria.andEndTimeGreaterThanOrEqualTo(now);
            criteria.andStockCountGreaterThan(0);//库存大于0
            tbSeckillGoods = seckillGoodsMapper.selectByExample(example);
            //更新到缓存
            for (TbSeckillGoods tbSeckillGood : tbSeckillGoods) {
                //将数据添加到缓存 ,id为小key,每件秒杀商品为value
                redisTemplate.boundHashOps("seckillGoods").put(tbSeckillGood.getId(), tbSeckillGood);
                System.out.println("将数据存入缓存" + tbSeckillGood.getId());
            }
        }


        return tbSeckillGoods;
    }


    /**
     * 根据id从内存中查询秒杀商品详情
     *
     * @param id
     * @return
     */
    @Override
    public TbSeckillGoods findByIdFromRedis(Long id) {
        return (TbSeckillGoods) redisTemplate.boundHashOps("seckillGoods").get(id);
    }

    @Autowired
    private IdWorker idWorker;

    /**
     * @param seckillId 秒杀商品id
     * @param userId    用户id
     */
    @Override
    public Long submitOrder(Long seckillId, String userId) {
        TbSeckillGoods seckillGoods = (TbSeckillGoods) redisTemplate.boundHashOps("seckillGoods").get(seckillId);//缓存中的商品
        if (seckillGoods == null) {
            throw new RuntimeException("商品不存在");
        }
        //库存没有
        if (seckillGoods.getStockCount() <= 0) {
            throw new RuntimeException("商品抢光");//不可在此保存数据库,会对数据库产生压力
        }
        seckillGoods.setStockCount(seckillGoods.getStockCount() - 1);  //库存-1
        redisTemplate.boundHashOps("seckillGoods").put(seckillId, seckillGoods);//更新缓存
        //库存没有
        if (seckillGoods.getStockCount() == 0) {
            seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);//更新数据库
            redisTemplate.boundHashOps("seckillGoods").delete(seckillId);//删除缓存
        }
        //构建订单
        TbSeckillOrder order = new TbSeckillOrder();
        long id = idWorker.nextId();
        order.setId(id);//主键
        order.setSeckillId(seckillId);//秒杀商品id
        order.setMoney(seckillGoods.getCostPrice());//支付金额
        order.setUserId(userId);//用户
        order.setSellerId(seckillGoods.getSellerId());//商家
        order.setCreateTime(new Date());//创建时间
        order.setStatus("0");//状态 未支付
        System.out.println(order);
        redisTemplate.boundHashOps("seckillOrder").put(userId, order);//订单缓存到redis
        return id;
    }


}
