package com.pinyougou.seckill.service.impl;


import com.alibaba.dubbo.config.annotation.Service;
import com.pinyougou.mapper.TbOrderItemMapper;
import com.pinyougou.mapper.TbSeckillGoodsMapper;
import com.pinyougou.mapper.TbSeckillOrderMapper;
import com.pinyougou.pojo.TbSeckillGoods;
import com.pinyougou.pojo.TbSeckillGoodsExample;
import com.pinyougou.pojo.TbSeckillOrder;

import com.pinyougou.seckill.service.SeckillOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Date;
import java.util.List;

/**
 * @program: pinyougou_parent
 * @name: SeckillOrderServiceImpl
 * @author;lin
 * @create: 2019-07-24 12:41
 **/
@Service
public class SeckillOrderServiceImpl implements SeckillOrderService {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 从缓存中查询订单
     *
     * @param userId
     * @return
     */
    @Override
    public TbSeckillOrder searchOrderFromRedis(String userId) {
        return (TbSeckillOrder) redisTemplate.boundHashOps("seckillOrder").get(userId);
    }


    @Autowired
    private TbSeckillOrderMapper tbSeckillOrderMapper;

    /**
     * 从缓存中读取订单
     *
     * @param userId
     * @param orderId
     * @param transactionId
     */
    @Override
    public void saveOrderFromRedisToDb(String userId, Long orderId, String transactionId) {
        TbSeckillOrder tbSeckillOrder = (TbSeckillOrder) redisTemplate.boundHashOps("seckillOrder").get(userId);//从缓存中读取订单
        if (tbSeckillOrder == null) {
            throw new RuntimeException("订单不存在");
        }
        if (orderId.longValue() != tbSeckillOrder.getId()) {
            throw new RuntimeException("订单不符");
        }
        tbSeckillOrder.setPayTime(new Date());//支付时间
        tbSeckillOrder.setTransactionId(transactionId);//交易流水号
        tbSeckillOrder.setStatus("1");//已支付
        tbSeckillOrderMapper.insert(tbSeckillOrder);//保存到数据库
        redisTemplate.boundHashOps("seckillOrder").delete(userId);//清除缓存
    }

    @Autowired
    private TbSeckillGoodsMapper tbSeckillGoodsMapper;

    /**
     * 客户未付款,从缓存中删除该订单,恢复库存数量
     *
     * @param userId
     * @param orderId
     */
    @Override
    public void deleteOrderFromRedis(String userId, Long orderId) {
        TbSeckillOrder tbSeckillOrder = (TbSeckillOrder) redisTemplate.boundHashOps("seckillOrder").get(userId);//从缓存中读取订单
        Long seckillId = tbSeckillOrder.getSeckillId();//商品id
        if (tbSeckillOrder != null && seckillId.longValue() == orderId) {
            redisTemplate.boundHashOps("seckillOrder").delete(userId);//删除缓存中的订单

            //恢复缓存中商品的数量
            TbSeckillGoods tbSeckillGoods = (TbSeckillGoods) redisTemplate.boundHashOps("seckillGoods").get(seckillId);//缓存中的商品
            if (tbSeckillGoods != null) {
                tbSeckillGoods.setStockCount(tbSeckillGoods.getStockCount() + 1);//库存数+1
                redisTemplate.boundHashOps("seckillGoods").put(seckillId, tbSeckillGoods);
            } else {
                //如果缓存中没有商品则需要从数据库查询一个,属性从数据库查询
                TbSeckillGoods seckillGoods = tbSeckillGoodsMapper.selectByPrimaryKey(seckillId);
                seckillGoods.setStockCount(1);//设置库存数量为1
                //从redis缓存一个
                redisTemplate.boundHashOps("seckillGoods").put(seckillId,seckillGoods);
            }
        }
    }










}




