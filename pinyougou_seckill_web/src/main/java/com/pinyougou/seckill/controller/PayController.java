package com.pinyougou.seckill.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.entity.Result;

import com.pinyougou.pay.service.WeixinPayService;
import com.pinyougou.pojo.TbPayLog;

import com.pinyougou.pojo.TbSeckillOrder;
import com.pinyougou.seckill.service.SeckillOrderService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @program: pinyougou_parent
 * @name: PayController
 * @author;lin
 * @create: 2019-07-22 22:44
 **/
@RestController
@RequestMapping("/pay")
public class PayController {
    @Reference(timeout = 5000)
    private WeixinPayService weixinPayService;

    @Reference(timeout = 5000)
    private SeckillOrderService seckillOrderService;


    /**
     * 生成二维码的参数
     *
     * @return id 为订单的id
     */
    @RequestMapping("/createNative")
    public Map createNative() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        TbSeckillOrder order = seckillOrderService.searchOrderFromRedis(username);
        if (order != null) {
            BigDecimal money = order.getMoney();//金额
            long price = (long) (money.doubleValue() * 100);
            Map map = weixinPayService.cerateNative(order.getId() + "", price + "");//得到支付的url
            return map;
        } else {
            Map map = new HashMap();
            return map;
        }

   /*     //得到out_trade_no
        System.out.println(out_trade_no);
        TbPayLog tbPayLog = orderService.selectPayLogFromRedis(out_trade_no);//从缓存中得到当前订单的支付日志
        if (tbPayLog != null) {
            Map map = weixinPayService.cerateNative(out_trade_no, tbPayLog.getTotalFee() + "");
            System.out.println(map);
            return map;
        } else {
            Map map = new HashMap();
            return map;
        }*/
    }


    /**
     * 询问当前支付状态
     *
     * @param out_trade_no
     * @return
     */
    @RequestMapping("/queryPayStatus")
    public Result queryPayStatus(String out_trade_no) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        System.out.println(username);
        Result result = new Result();
        int i = 0;
        while (true) {
            Map<String, String> map = weixinPayService.queryPayStatus(out_trade_no);
            if (map == null) {
                result = new Result(false, "支付出错");
                break;
            }
            if ("SUCCESS".equals(map.get("trade_state"))) {
                result = new Result(true, "支付成功");
                //支付成功后清除日志的缓存
                /*  orderService.updateOrderStatus(out_trade_no, map.get("transaction_id"));*/
                seckillOrderService.saveOrderFromRedisToDb(username, Long.valueOf(out_trade_no), map.get("transaction_id"));
                break;


            }
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            i++;
            if (i >= 100) {
                result = new Result(false, "二维码超时");
                //调用微信关闭
                Map closeMap = weixinPayService.closePay(out_trade_no);
                if (!"SUCCESS".equals(closeMap.get("result_code"))) {//如果返回结果是正常关闭
                    if ("ORDERPAID".equals(closeMap.get("err_code"))) {//订单已经支付
                        result = new Result(true, "支付成功");
                        seckillOrderService.saveOrderFromRedisToDb(username,
                                Long.valueOf(out_trade_no), map.get("transaction_id"));
                    }
                }
                if (result.isSuccess() == false) {
                    System.out.println("超时，取消订单");
                    //2.调用删除,从redis中删除
                    seckillOrderService.deleteOrderFromRedis(username,
                            Long.valueOf(out_trade_no));
                }

                break;
            }

        }
        return result;


    }

    ;


}






