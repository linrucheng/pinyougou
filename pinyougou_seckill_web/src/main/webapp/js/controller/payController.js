app.controller('payController', function ($scope, payService, $location) {

      //获得结算页提交的支付订单号out_trade_no
    getOutTradeNo = function () {
        return $location.search()['out_trade_no']
    }


    $scope.createNative = function () {
        payService.createNative().success(
            function (response) {
                $scope.money = (response.total_fee / 100).toFixed(2); //金额
                $scope.out_trade_no = response.out_trade_no;//订单号
                //二维码
                var qr = new QRious({
                    element: document.getElementById('qrious'),
                    size: 280,
                    level: 'M',
                    value: response.code_url
                });
                queryPayStatus($scope.out_trade_no)
            }
        );
    }



    queryPayStatus = function (out_trade_no) {
        payService.queryPayStatus(out_trade_no).success(
            function (response) {
                if (response.success) {
                    location.href = "paysuccess.html#?money=" + $scope.money;//发送给支付成功页面发送当前的支付金额
                } else {
                    if (response.message = '二维码超时') {
                       alert("支付超时")
                        location.href="seckill-index.html"
                    } else {
                        location.href = "payfail.html"
                    }
                }
            })

    }


    //获得当前支付的金额
    $scope.getTotalMoney = function () {
        return $location.search()['money']//接收页面传来的金额
    }


});



