//控制层
app.controller('seckillGoodsController', function ($scope, $location, seckillGoodsService, $interval) {
//读取列表数据绑定到表单中
    $scope.findList = function () {
        seckillGoodsService.findList().success(
            function (response) {
                $scope.list = response;
            }
        );
    }

    //根据id查询秒杀商品详情
    //seckillGoodsService
    $scope.findById = function () {
        // alert($location.search()['id'])
        seckillGoodsService.findOne($location.search()['id']).success(
            function (response) {
                $scope.entity = response;
            });

    }


    //查询实体
    $scope.findOne = function () {
        seckillGoodsService.findOne($location.search()['id']).success(
            function (response) {
                $scope.entity = response;

                allsecond = Math.floor((new Date($scope.entity.endTime).getTime() - new Date().getTime()) / 1000);//总秒数
                time = $interval(function () {
                    if (allsecond > 0) {
                        allsecond = allsecond - 1;
                        $scope.timeString = convertTimeString(allsecond);
                    } else {
                        $interval.cancel(time);
                        alert("秒杀结束")
                    }

                }, 1000)

            }
        );
    }
    //将时间转化
    convertTimeString = function (allsecond) {
        var day = Math.floor(allsecond / (60 * 60 * 24))//天数
        var hour = Math.floor((allsecond - day * 60 * 60 * 24) / (60 * 60))//小时
        var minute = Math.floor((allsecond - day * 60 * 60 * 24 - hour * 60 * 60) / 60)//分钟
        var second = allsecond - day * 60 * 60 * 24 - hour * 60 * 60 - 60 * minute//秒
        var timeString = ""
        if (day > 0) {
            timeString = day + "天"
        }
        return timeString + hour + ":" + minute + ":" + second;
    }

    //提交订单
    $scope.submitOrder = function () {
        seckillGoodsService.submitOrder($scope.entity.id).success(
            function (response) {
                if (response.success) {
                    alert("请在5分钟之内付款")
                    //alert(response.orderId)
                    location.href = "pay.html"
                } else {
                    if (response.message=="请登录"){
                        alert("请登录")
                        location.href="login.html"
                    }else {
                        alert(response.message)
                    }

                }
            })
    }


});