app.service('seckillGoodsService', function ($http) {
//读取列表数据绑定到表单中seckillGoodsService
    this.findList = function () {
        return $http.get('seckillGoods/findList.do');
    }


    //查询实体
    this.findOne = function (id) {
        return $http.get('../seckillGoods/findByIdFromRedis.do?id=' + id);
    }
    //提交订单
    this.submitOrder = function (id) {
        return $http.get('../seckillGoods/submitOrder.do?seckillId=' + id);
    }



});