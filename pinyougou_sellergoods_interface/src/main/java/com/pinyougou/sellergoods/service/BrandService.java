package com.pinyougou.sellergoods.service;

import com.pinyougou.entity.PageResult;
import com.pinyougou.pojo.TbBrand;

import java.util.List;
import java.util.Map;

public interface BrandService {
    /*品牌服务层接口*/
    public List<TbBrand> findAll();

    //品牌分页查询
    public PageResult findByPage(int currentPage, int pageSize);


    public void add(TbBrand tbBrand);  //品牌的增加


    public TbBrand findOne(Long id);//根据id查询一个

    public void update(TbBrand tbBrand);//修改

    public void delete(Long[] ids);//删除

    public PageResult findByPage(int currentPage, int pageSize, TbBrand tbBrand);//根据条件查询
    public List<Map>selectBrandList();
}

