package com.pinyougou.sellergoods.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pinyougou.entity.PageResult;
import com.pinyougou.mapper.TbBrandMapper;
import com.pinyougou.pojo.TbBrand;
import com.pinyougou.pojo.TbBrandExample;
import com.pinyougou.sellergoods.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @program: pinyougou_parent
 * @name: BrandServiceImpl
 * @author;lin
 * @create: 2019-06-28 12:52
 * 品牌服务层接口实现
 **/
@Transactional
@Service
public class BrandServiceImpl implements BrandService {
    @Autowired
    private TbBrandMapper tbBrandMapper;

    @Override
    public List<TbBrand> findAll() {
        //查询所有的品牌
        return tbBrandMapper.selectByExample(null);
    }

    @Override
    public PageResult findByPage(int currentPage, int pageSize) {
        //品牌分页查询
        PageHelper.startPage(currentPage, pageSize);
        List<TbBrand> list = tbBrandMapper.selectByExample(null);

        PageInfo<TbBrand> info = new PageInfo<TbBrand>(list);
        PageResult<TbBrand> pageResult = new PageResult<TbBrand>();

        pageResult.setRows(info.getList());
        return pageResult;
    }

    @Override
    public void add(TbBrand tbBrand) {
        //品牌的增加
        tbBrandMapper.insert(tbBrand);
    }

    @Override
    public TbBrand findOne(Long id) {
        return tbBrandMapper.selectByPrimaryKey(id);//根据id查询一个
    }

    @Override
    public void update(TbBrand tbBrand) {
        //更新
        tbBrandMapper.updateByPrimaryKey(tbBrand);
    }

    @Override
    public void delete(Long[] ids) {
        //删除
        for (Long id : ids) {
            tbBrandMapper.deleteByPrimaryKey(id);
        }
    }

    @Override
    public PageResult findByPage(int currentPage, int pageSize, TbBrand tbBrand) {
        //分页+查询
        PageHelper.startPage(currentPage, pageSize);
        TbBrandExample example = new TbBrandExample();//查询条件
        TbBrandExample.Criteria criteria = example.createCriteria();
        if (tbBrand.getName() != null && tbBrand.getName().length() > 0) {
            //名字不为空
            criteria.andNameLike("%" + tbBrand.getName() + "%");
        }
        if (tbBrand.getFirstChar() != null && tbBrand.getFirstChar().length() > 0) {
            //首字母不为空
            criteria.andFirstCharEqualTo(tbBrand.getFirstChar());

        }
        List<TbBrand> list = tbBrandMapper.selectByExample(example);
        PageInfo<TbBrand> info = new PageInfo<TbBrand>(list);
        PageResult<TbBrand> pageResult = new PageResult<TbBrand>();
        //将info的属性封装到pageResult对象中
        pageResult.setTotal(info.getTotal());
        pageResult.setRows(list);
        return pageResult;
    }

    @Override
    public List<Map> selectBrandList() {
        return tbBrandMapper.selectBrandList();
    }


}




