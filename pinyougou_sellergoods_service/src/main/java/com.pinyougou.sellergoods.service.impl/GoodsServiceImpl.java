package com.pinyougou.sellergoods.service.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.pinyougou.mapper.*;
import com.pinyougou.pojo.*;
import com.pinyougou.pojoGroup.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.pinyougou.pojo.TbGoodsExample.Criteria;
import com.pinyougou.sellergoods.service.GoodsService;

import com.pinyougou.entity.PageResult;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;


/**
 * 服务实现层
 *
 * @author Administrator
 */
@Transactional
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private TbGoodsMapper goodsMapper;
    @Autowired
    private TbGoodsDescMapper tbGoodsDescMapper;
    @Autowired
    private TbBrandMapper brandMapper;

    /**
     * 查询全部
     */
    @Override
    public List<TbGoods> findAll() {
        return goodsMapper.selectByExample(null);
    }

    /**
     * 按分页查询
     */
    @Override
    public PageResult findPage(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        Page<TbGoods> page = (Page<TbGoods>) goodsMapper.selectByExample(null);
        return new PageResult(page.getTotal(), page.getResult());
    }

    /**
     * 增加
     */
    @Autowired
    private TbItemCatMapper tbItemCatMapper;

    @Autowired
    private TbSellerMapper tbSellerMapper;
    @Autowired
    private TbItemMapper tbItemMapper;

    @Override
    public void add(Goods goods) {
        //对tb_goods添加
        goods.getTbGoods().setAuditStatus("0");
        goodsMapper.insert(goods.getTbGoods());
        //对tb_goods_desc添加
        goods.getTbGoodsDesc().setGoodsId(goods.getTbGoods().getId());
        tbGoodsDescMapper.insert(goods.getTbGoodsDesc());
        //对tb_item表添加
        addItem(goods);

    }

    /**
     * 添加item表
     *
     * @param goods
     */

    private void addItem(Goods goods) {
        //对tb_item添加
        List<TbItem> items = goods.getTbItems();

        if ("1".equals(goods.getTbGoods().getIsEnableSpec())) {
            for (TbItem item : items) {
                //标题 苹果(Apple) iPhone 5s (A1533) 16GB 银色 电信3G手机
                // +商品名称+spec
                String title = goods.getTbGoods().getGoodsName();
                //{"机身内存":"16G","网络":"联通2G"}将json转化为map
                Map<String, Object> map = JSON.parseObject(item.getSpec());
                for (String key : map.keySet()) {
                    title = title + " " + map.get(key);
                }
                item.setTitle(title);

                setItemValue(item, goods);
                tbItemMapper.insert(item);
            }
        } else {
            //不启动规格,设置一个默认规格
            TbItem item = new TbItem();
            item.setTitle(goods.getTbGoods().getGoodsName());//标题
            item.setPrice(goods.getTbGoods().getPrice());//价格
            item.setStatus("1");//状态
            item.setNum(9999);//库存
            item.setIsDefault("1");//是否默认
            item.setSpec("{}");//规格

            setItemValue(item, goods);
            tbItemMapper.insert(item);


        }

    }

    /**
     * 假如没有启用规格,对规格设置一个默认值
     *
     * @param item
     * @param goods
     */

    public void setItemValue(TbItem item, Goods goods) {
        //goodId,为goodId
        item.setGoodsId(goods.getTbGoods().getId());
        //Sellerid 商家id
        item.setSellerId(goods.getTbGoods().getSellerId());
        //categoryId三级分类的id
        item.setCategoryid(goods.getTbGoods().getCategory3Id());
        //创建时间
        item.setCreateTime(new Date());
        //更新时间
        item.setUpdateTime(new Date());
        //品牌名称
        TbBrand brand = brandMapper.selectByPrimaryKey(goods.getTbGoods().getBrandId());
        item.setBrand(brand.getName());
        //分类名称
        TbItemCat itemCat = tbItemCatMapper.selectByPrimaryKey(goods.getTbGoods().getCategory3Id());
        item.setCategory(itemCat.getName());
        //商家名称
        TbSeller seller = tbSellerMapper.selectByPrimaryKey(goods.getTbGoods().getSellerId());
        item.setSeller(seller.getNickName());
        //图片取tb_goods_desc表的图片的第一个图片;http://192.168.25.133/group1/M00/00/00/wKgZhVnGZfWAaX2hAAjlKdWCzvg173.jpg
        //是数组类型的字符串,转化为list<Map>结构
        //[{"color":"白色","url":"http://192.168.25.133/group1/M00/00/00/wKgZhVnGbYuAO6AHAAjlKdWCzvg253.jpg"},
        //{"color":"蓝色","url":"http://192.168.25.133/group1/M00/00/00/wKgZhVnKX4yAbCC0AAFa4hmtWek406.jpg"},
        // {"color":"黑色","url":"http://192.168.25.133/group1/M00/00/00/wKgZhVnKX5WAOsqXAAETwD7A1Is409.jpg"}]
        List<Map> imageList = JSON.parseArray(goods.getTbGoodsDesc().getItemImages(), Map.class);
        if (imageList != null && imageList.size() > 0) {
            item.setImage((String) imageList.get(0).get("url"));

        }


    }


    /**
     * 修改
     *
     * @param goods
     */
    @Override
    public void update(Goods goods) {
        //修改tb_goods表,由于修改了表,需要将状态设置为0未审核状态,让运营商重新审核
        goods.getTbGoods().setAuditStatus("0");
        goodsMapper.updateByPrimaryKey(goods.getTbGoods());
        //修改tb_goods_desc表
        tbGoodsDescMapper.updateByPrimaryKey(goods.getTbGoodsDesc());
        //修改tb_item表,由于是集合,采用先删除后添加
        List<TbItem> tbItems = goods.getTbItems();
        //先删除
        TbItemExample example = new TbItemExample();
        TbItemExample.Criteria criteria = example.createCriteria();
        criteria.andGoodsIdEqualTo(goods.getTbGoods().getId());
        tbItemMapper.deleteByExample(example);
        //后添加
        addItem(goods);
    }


    /**
     * 根据ID获取实体
     *
     * @param id
     * @return
     */

    @Override
    public Goods findOne(Long id) {
        Goods goods = new Goods();

        //商品spu
        TbGoods tbGoods = goodsMapper.selectByPrimaryKey(id);
        goods.setTbGoods(tbGoods);
        //商品spu扩展表
        TbGoodsDesc tbGoodsDesc = tbGoodsDescMapper.selectByPrimaryKey(id);
        goods.setTbGoodsDesc(tbGoodsDesc);
        //商品sku表,sku的goods_id和good表的id相等
        TbItemExample example = new TbItemExample();
        TbItemExample.Criteria criteria = example.createCriteria();
        criteria.andGoodsIdEqualTo(id);
        List<TbItem> tbItems = tbItemMapper.selectByExample(example);
        goods.setTbItems(tbItems);
        return goods;
    }







    /**
     * 批量删除
     */
    @Override
    public void delete(Long[] ids) {
        for (Long id : ids) {
            //tb_goods的isdelect字段设置为1
            TbGoods tbGoods = goodsMapper.selectByPrimaryKey(id);
            tbGoods.setIsDelete("1");
            goodsMapper.updateByPrimaryKey(tbGoods);

        }
    }


    @Override
    public PageResult findPage(TbGoods goods, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);

        TbGoodsExample example = new TbGoodsExample();
        Criteria criteria = example.createCriteria();
        //增加条件,查询idDelete字段为null的
        criteria.andIsDeleteIsNull();
        if (goods != null) {
            if (goods.getSellerId() != null && goods.getSellerId().length() > 0) {
                criteria.andSellerIdEqualTo(goods.getSellerId());
            }
            if (goods.getGoodsName() != null && goods.getGoodsName().length() > 0) {
                criteria.andGoodsNameLike("%" + goods.getGoodsName() + "%");
            }
            if (goods.getAuditStatus() != null && goods.getAuditStatus().length() > 0) {
                criteria.andAuditStatusLike("%" + goods.getAuditStatus() + "%");
            }
            if (goods.getIsMarketable() != null && goods.getIsMarketable().length() > 0) {
                criteria.andIsMarketableLike("%" + goods.getIsMarketable() + "%");
            }
            if (goods.getCaption() != null && goods.getCaption().length() > 0) {
                criteria.andCaptionLike("%" + goods.getCaption() + "%");
            }
            if (goods.getSmallPic() != null && goods.getSmallPic().length() > 0) {
                criteria.andSmallPicLike("%" + goods.getSmallPic() + "%");
            }
            if (goods.getIsEnableSpec() != null && goods.getIsEnableSpec().length() > 0) {
                criteria.andIsEnableSpecLike("%" + goods.getIsEnableSpec() + "%");
            }
            if (goods.getIsDelete() != null && goods.getIsDelete().length() > 0) {
                criteria.andIsDeleteLike("%" + goods.getIsDelete() + "%");
            }

        }

        Page<TbGoods> page = (Page<TbGoods>) goodsMapper.selectByExample(example);
        return new PageResult(page.getTotal(), page.getResult());
    }

    /**
     * 提交审核
     *
     * @param ids
     */

    @Override
    public Boolean updateStatus(Long[] ids) {
        if ("".equals(ids) || ids.length == 0) {
            return false;
        }
        for (Long id : ids) {
            TbGoods tbGoods = goodsMapper.selectByPrimaryKey(id);
            if ("0".equals(tbGoods.getAuditStatus())) {
                //查找"状态为0 未审核,修改状态为1审核中
                tbGoods.setAuditStatus("1");
                //修改参数为1
                goodsMapper.updateByPrimaryKeySelective(tbGoods);

            } else {
                return false;
            }

        }
        return true;

    }

    /*修改sh状态
     *
     * @param ids
     * @param status
     */

    @Override
    public void updateStatus(Long[] ids, String status) {
        if ("".equals(ids) || ids.length == 0) {
            return;
        }

        for (Long id : ids) {
            TbGoods tbGoods = goodsMapper.selectByPrimaryKey(id);
            //对于审核通过的商品,默认为上架状态
            if ("2".equals(status)) {
                tbGoods.setIsMarketable("0");
            }


            tbGoods.setAuditStatus(status);
            goodsMapper.updateByPrimaryKeySelective(tbGoods);
        }

    }

    /**
     * 修改上架状态
     *
     * @param ids
     * @param status
     * @return
     */
    @Override
    public Boolean updateIsMarketable(Long[] ids, String status) {
        if ("".equals(ids) || ids.length == 0) {
            return false;
        }
        for (Long id : ids) {
            TbGoods tbGoods = goodsMapper.selectByPrimaryKey(id);
            if ("2".equals(tbGoods.getAuditStatus())) {
                //商品是处于审核通过状态
                tbGoods.setIsMarketable(status);
                goodsMapper.updateByPrimaryKeySelective(tbGoods);
            } else {
                return false;
            }
        }
        return true;

    }

    //根据ids查询goods的所有数据

    public List<TbGoods> findByIds(Long[] ids) {
        if (ids!=null&&ids.length>0){
            TbGoodsExample example = new TbGoodsExample();
            Criteria criteria = example.createCriteria();
            criteria.andIdIn(Arrays.asList(ids));
            return goodsMapper.selectByExample(example);
        } else {
            return null;
        }
    }



}
