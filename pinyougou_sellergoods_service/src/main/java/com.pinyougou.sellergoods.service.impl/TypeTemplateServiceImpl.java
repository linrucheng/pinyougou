package com.pinyougou.sellergoods.service.impl;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.pinyougou.mapper.TbSpecificationOptionMapper;
import com.pinyougou.pojo.TbSpecificationOption;
import com.pinyougou.pojo.TbSpecificationOptionExample;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.pinyougou.mapper.TbTypeTemplateMapper;
import com.pinyougou.pojo.TbTypeTemplate;
import com.pinyougou.pojo.TbTypeTemplateExample;
import com.pinyougou.pojo.TbTypeTemplateExample.Criteria;
import com.pinyougou.sellergoods.service.TypeTemplateService;

import com.pinyougou.entity.PageResult;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;


/**
 * 服务实现层
 *
 * @author Administrator
 */
@Transactional
@Service
public class TypeTemplateServiceImpl implements TypeTemplateService {

    @Autowired
    private TbTypeTemplateMapper typeTemplateMapper;

    /**
     * 查询全部
     */
    @Override
    public List<TbTypeTemplate> findAll() {
        return typeTemplateMapper.selectByExample(null);
    }

    /**
     * 按分页查询
     */
    @Override
    public PageResult findPage(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        Page<TbTypeTemplate> page = (Page<TbTypeTemplate>) typeTemplateMapper.selectByExample(null);
        return new PageResult(page.getTotal(), page.getResult());
    }

    /**
     * 增加
     */
    @Override
    public void add(TbTypeTemplate typeTemplate) {
        typeTemplateMapper.insert(typeTemplate);
    }


    /**
     * 修改
     */
    @Override
    public void update(TbTypeTemplate typeTemplate) {
        typeTemplateMapper.updateByPrimaryKey(typeTemplate);
    }

    /**
     * 根据ID获取实体
     *
     * @param id
     * @return
     */
    @Override
    public TbTypeTemplate findOne(Long id) {
        return typeTemplateMapper.selectByPrimaryKey(id);
    }

    /**
     * 批量删除
     */
    @Override
    public void delete(Long[] ids) {
        for (Long id : ids) {
            typeTemplateMapper.deleteByPrimaryKey(id);
        }
    }


    @Override
    public PageResult findPage(TbTypeTemplate typeTemplate, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);

        TbTypeTemplateExample example = new TbTypeTemplateExample();
        Criteria criteria = example.createCriteria();

        if (typeTemplate != null) {
            if (typeTemplate.getName() != null && typeTemplate.getName().length() > 0) {
                criteria.andNameLike("%" + typeTemplate.getName() + "%");
            }
            if (typeTemplate.getSpecIds() != null && typeTemplate.getSpecIds().length() > 0) {
                criteria.andSpecIdsLike("%" + typeTemplate.getSpecIds() + "%");
            }
            if (typeTemplate.getBrandIds() != null && typeTemplate.getBrandIds().length() > 0) {
                criteria.andBrandIdsLike("%" + typeTemplate.getBrandIds() + "%");
            }
            if (typeTemplate.getCustomAttributeItems() != null && typeTemplate.getCustomAttributeItems().length() > 0) {
                criteria.andCustomAttributeItemsLike("%" + typeTemplate.getCustomAttributeItems() + "%");
            }

        }

        Page<TbTypeTemplate> page = (Page<TbTypeTemplate>) typeTemplateMapper.selectByExample(example);
        saveToRedis();
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 缓存所有的品牌和规格
     */
    private void saveToRedis() {
        //查询所有的模板
        List<TbTypeTemplate> templates = findAll();

        for (TbTypeTemplate template : templates) {
            Long id = template.getId();
            //{id:35,name:手机,spec_ids:[{"id":27,"text":"网络"},{"id":32,"text":"机身内存"}],brand_ids:[{"text":"内存大小"},{"text":"颜色"}]}
            List<Map> brandList = JSON.parseArray(template.getBrandIds(), Map.class);//获取所有的品牌
            //模板id作为key,品牌作为value,缓存品牌
            redisTemplate.boundHashOps("brandList").put(id,brandList);
            System.out.println("缓存品牌");
            //模板id作为key,规格作为value,缓存规格,规格[{"id":27,"text":"网络"},{"id":32,"text":"机身内存"}]没有规格选项,所以要重新查询
            List<Map> specList = findTemplateList(id);
            //缓存规格
            System.out.println("缓存规格");
            redisTemplate.boundHashOps("specList").put(id,specList);
        }




    }


    /**
     * 查询所有的模板
     *
     * @return
     */

    @Override
    public List<Map> findTemplateList() {


        return typeTemplateMapper.findTemplateList();
    }

    /**
     * 根据模板的id查询所有规格和规格选项
     *
     * @param id
     * @return
     */
    @Autowired
    private TbSpecificationOptionMapper specificationOptionMapper;

    @Override
    public List<Map> findTemplateList(Long id) {
        //查询模板
        TbTypeTemplate template = typeTemplateMapper.selectByPrimaryKey(id);
        //规格
        String specIds = template.getSpecIds();
        //将字符串转化为json格式
        //[{"id":27,"text":"网络"},{"id":32,"text":"机身内存"},{"id":28,"text":"手机屏幕尺寸"},{"id":34,"text":"cpu"}]
        List<Map> specifications = JSON.parseArray(specIds, Map.class);
        for (Map specification : specifications) {
            //得到规格的id
            Long specId = new Long((Integer) specification.get("id"));
            //在从tb_specificationch查询所有的规格选项
            TbSpecificationOptionExample example = new TbSpecificationOptionExample();
            TbSpecificationOptionExample.Criteria criteria = example.createCriteria();
            criteria.andSpecIdEqualTo(specId);
            //所有的规格选项
            List<TbSpecificationOption> options = specificationOptionMapper.selectByExample(example);
            //添加到map集合中得到的最终结果格式为{"id":27,"text":"网络","option":[]}
            // [{"id":27,"text":"网络","option":[]},{"id":32,"text":"机身内存","option":[]}]
            specification.put("option", options);
        }

        return specifications;
    }


    ;


}
