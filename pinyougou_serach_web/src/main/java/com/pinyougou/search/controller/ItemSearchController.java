package com.pinyougou.search.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.search.service.ItemSearchService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @program: pinyougou_parent
 * @name: SearchController
 * @author;lin
 * @create: 2019-07-10 19:54
 **/
@RestController
@RequestMapping("/item")
public class ItemSearchController {
    @Reference
    private ItemSearchService itemSearch;

    /**
     * 查询
     *
     * @param searchMap
     * @return
     */
    @RequestMapping("/search")
    public Map<String, Object> itemSearch(@RequestBody Map<String, Object> searchMap) {
        return itemSearch.search(searchMap);
    }


}




