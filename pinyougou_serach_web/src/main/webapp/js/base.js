var app = angular.module('pinyougou', [])

/**/
app.filter('trustHtml', function ($sce) {
    return function (data) {
        return $sce.trustAsHtml(data)//设置过滤器,将后台传过来的数据当做html处理
    }
})