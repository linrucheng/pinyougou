//控制层
app.controller('serachcontroller', function (serachService, $location, $scope) {

    $scope.serach = function () {
        $scope.searchMap.currentPage = parseInt($scope.searchMap.currentPage)


        serachService.serach($scope.searchMap).success(
            function (rseponse) {
                $scope.result = rseponse
                $scope.resultMap = rseponse
                /*如果查询到总页数小于当前页,则将当前页置为1*/
                if ($scope.resultMap.totalPages < $scope.searchMap.currentPage) {
                    $scope.searchMap.currentPage = 1
                    $scope.serach();
                }


                buildPage()
            })


    }

    //构建查询条件
    //关键字,分类,品牌,规格
    $scope.searchMap = {
        'keywords': '',
        'category': '',
        'brand': '',
        'spec': {},
        'price': '',
        'currentPage': 1,
        'pageSize': 10,
        'sort': '', /*排序方式*/
        'sortField': ''/*排序字段*/

    };

    //向查询条件里增加条件
    $scope.addSerach = function (key, value) {
        //如果查询条件等于品牌和分类
        if (key == 'category' || key == "brand" || key == 'price') {
            $scope.searchMap[key] = value
        } else {
            $scope.searchMap.spec[key] = value
        }
        $scope.serach();

    }

    //向查询条件里移除条件
    $scope.deleSerach = function (key) {
        //如果查询条件等于品牌和分类
        if (key == 'category' || key == "brand" || key == 'price') {
            $scope.searchMap[key] = ''
        } else {
            //移除次属性
            delete $scope.searchMap.spec[key]
        }
        $scope.serach();
    }

    //分页,前2后2,共计5条
    buildPage = function () {
        $scope.pageArray = [];

        var totalPage = $scope.resultMap.totalPages;//总页数
        var start = 1
        var end = totalPage;
        var currentPage = $scope.searchMap.currentPage;//当前页


        /*如果当前页大于总页数*/


        if (totalPage <= 5) {
            //如果总页数小于5,则显示全部
            start = 1;
            end = totalPage;
            $scope.firstDot = false;
            $scope.lastDot = false;

        } else {
            //假如总页数大于5
            if (currentPage - 2 <= 1) {
                //如果当前页小于2
                start = 1;
                end = 5;
                /*前面后面都没有点*/
                $scope.firstDot = false;
                $scope.lastDot = false;

            } else if (currentPage + 2 >= totalPage) {
                //如果当前页+2大于总页数
                start = totalPage - 4;
                end = totalPage;
                /*前面有点后面无点*/
                $scope.firstDot = true;
                $scope.lastDot = false;

            } else {
                /*前面后面都有点*/
                $scope.firstDot = true;
                $scope.lastDot = true;
                start = currentPage - 2;
                end = currentPage + 2;
            }
        }

        for (var i = start; i <= end; i++) {
            $scope.pageArray.push(i);
        }

    }

    //根据页码查询
    $scope.queryByPage = function (currentPage) {

        if (currentPage < 1 || currentPage > $scope.resultMap.totalPages) {
            return
        }
        $scope.searchMap.currentPage = currentPage;
        $scope.serach();
    }
    //上一页
    $scope.previousPage = function () {
        $scope.queryByPage($scope.searchMap.currentPage - 1);
    }
    //下一页
    $scope.nextPage = function () {
        $scope.queryByPage($scope.searchMap.currentPage + 1);
    }
    //跳到某页
    $scope.selectPage = function () {
        var currentPage = parseInt($scope.searchMap.currentPage)
        $scope.queryByPage(currentPage)
    }
    //设置排序规则
    $scope.sortSearch = function (sort, sortField) {
        $scope.searchMap.sort = sort;
        $scope.searchMap.sortField = sortField;
        $scope.serach();
    }

    //是否高亮显示
    $scope.isActive = function (sort, sortField) {
        if ($scope.searchMap.sort == sort && $scope.searchMap.sortField == sortField) {
            return true;
        } else {
            return false;
        }
    }

    //判断搜索的关键字包不包含品牌

    $scope.isBrand = function () {
        for (var i = 0; i < $scope.resultMap.brandList.length; i++) {
            if ($scope.searchMap.keywords.indexOf($scope.resultMap.brandList[i].text) >= 0) {
                return false;//包含品牌,不显示
            }
        }

        return true;//不包含品牌,显示
    }
    $scope.loadKeywords = function () {
        var keywords = $location.search()['keywords'];
        if (keywords =="undefined") {
            return;
        }
        $scope.searchMap.keywords = keywords
        $scope.serach()
    }






})





