package com.pinyougou.shop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.entity.PageResult;
import com.pinyougou.entity.Result;
import com.pinyougou.pojo.TbGoods;
import com.pinyougou.pojoGroup.Goods;
import com.pinyougou.search.service.ItemSearchService;
import com.pinyougou.sellergoods.service.GoodsService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * controller
 *
 * @author Administrator
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Reference
    private GoodsService goodsService;

    /**
     * 返回全部列表
     *
     * @return
     */
    @RequestMapping("/findAll")
    public List<TbGoods> findAll() {
        return goodsService.findAll();
    }


    /**
     * 返回全部列表
     *
     * @return
     */
    @RequestMapping("/findPage")
    public PageResult findPage(int page, int rows) {
        return goodsService.findPage(page, rows);
    }

    /**
     * 增加
     *
     * @param goods
     * @return
     */
    @RequestMapping("/add")
    public Result add(@RequestBody Goods goods) {
        //获取当前的用户名
        String sellerId = SecurityContextHolder.getContext().getAuthentication().getName();
        goods.getTbGoods().setSellerId(sellerId);
        try {
            goodsService.add(goods);
            return new Result(true, "增加成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "增加失败");
        }
    }


    /**
     * 修改
     *
     * @param goods
     * @return
     */
    @RequestMapping("/update")
    public Result update(@RequestBody Goods goods) {
        //校验当前商品的id是否是商家的id
        Long id = goods.getTbGoods().getId();
        Goods goods1 = goodsService.findOne(id);
        //数据库存储的商家
        String sellerId = goods1.getTbGoods().getSellerId();
        //当前操作的商家
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        //传递过来的商家id
        String sellerId1 = goods.getTbGoods().getSellerId();

        //先判断当前商家id是否是登录id,在判断当前登录的id和数据库存储的id是否相等
        if (!name.equals(sellerId1) || !name.equals(sellerId)) {
            return new Result(false, "非法操作");

        }

        try {
            goodsService.update(goods);
            return new Result(true, "修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "修改失败");
        }
    }

    /**
     * 获取实体
     *
     * @param id
     * @return
     */
    @RequestMapping("/findOne")
    public Goods findOne(Long id) {
        return goodsService.findOne(id);
    }









    /**
     * 查询+分页
     *
     * @param
     * @param page
     * @param rows
     * @return
     */
    @RequestMapping("/search")
    public PageResult search(@RequestBody TbGoods goods, int page, int rows) {
        //商家只能操作自己的产品

        String sellId = SecurityContextHolder.getContext().getAuthentication().getName();
        goods.setSellerId(sellId);
        return goodsService.findPage(goods, page, rows);
    }

    /**
     * 提交审核
     *
     * @param ids
     * @return
     */
    @RequestMapping("/updateStatus")
    public Result updateSattus(Long[] ids) {
        try {
            Boolean status = goodsService.updateStatus(ids);
            if (!status) {
                return new Result(true, "数据异常");
            }
            return new Result(true, "申请成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "申请失败");
        }
    }

    /**
     * 商品是否上架
     *
     * @param ids
     * @param status
     * @return
     */
    @RequestMapping("/updateIsMarketable")
    public Result updateIsMarketable(Long[] ids, String status) {
        try {
            Boolean flag = goodsService.updateIsMarketable(ids, status);

            if (!flag) {
                return new Result(false, "提交数据有误,请认真核对");
            }

            return new Result(true, "修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "修改失败");
        }
    }
    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @Reference(timeout = 5000)
    private ItemSearchService itemSearchService;

    @RequestMapping("/delete")
    public Result delete(Long[] ids) {
        List list = new ArrayList();//审核通过的商品id
        try {
            List<TbGoods> tbGoods = goodsService.findByIds(ids);
            for (TbGoods tbGood : tbGoods) {
                //判断是否是审核通过的商品
                if ("2".equals(tbGood.getAuditStatus())) {
                    list.add(tbGood.getId());
                }
            }
            itemSearchService.deleteByGoodIds(list);
            goodsService.delete(ids);

            return new Result(true, "删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "删除失败");
        }
    }


}
