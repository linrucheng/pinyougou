package com.pinyougou.shop.controller;

import com.pinyougou.entity.Result;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @program: pinyougou_parent
 * @name: LoginController
 * @author;lin
 * @create: 2019-07-02 18:27
 * 和登录相关
 **/
@RequestMapping("/login")
@RestController
public class LoginController {


    @RequestMapping("/getUsername.do")
    public Map getUsername() {
        //获取登录用户名
        String user = SecurityContextHolder.getContext().getAuthentication().getName();
        Map map = new HashMap();
        map.put("user", user);


        return map;
    }


}




