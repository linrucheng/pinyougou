package com.pinyougou.shop.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pojo.TbSeller;
import com.pinyougou.sellergoods.service.SellerService;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: pinyougou_parent
 * @name: UserServiceDetailImpl
 * @author;lin
 * @create: 2019-07-02 22:51
 **/
public class UserServiceDetailImpl implements UserDetailsService {
    @Reference
    private SellerService sellerService;//从service层引用

    public void setSellerService(SellerService sellerService) {
        this.sellerService = sellerService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //String username, String password,
        //Collection<? extends GrantedAuthority> authorities
        //创建角色
        List<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
        SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority("ROLE_ADMIN");
        authorities.add(simpleGrantedAuthority);
        TbSeller tbSeller = sellerService.findOne(username);
        if (tbSeller != null) {
            if (tbSeller.getStatus().equals("1")) {
                User user = new User(username, tbSeller.getPassword(), authorities);
                return user;
            } else {
                return null;
            }
        } else {
            return null;
        }


    }


}




