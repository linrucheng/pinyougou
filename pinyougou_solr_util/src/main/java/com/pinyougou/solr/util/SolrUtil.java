package com.pinyougou.solr.util;

import com.alibaba.fastjson.JSON;
import com.pinyougou.mapper.TbItemMapper;
import com.pinyougou.pojo.TbItem;
import com.pinyougou.pojo.TbItemExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @program: pinyougou_parent
 * @name: SolrUtil
 * @author;lin
 * @create: 2019-07-10 18:25
 **/
@Component
public class SolrUtil {
    @Autowired
    private TbItemMapper tbItemMapper;
    @Autowired
    private SolrTemplate solrTemplate;

    public void importItemDate() {
        TbItemExample example = new TbItemExample();
        TbItemExample.Criteria criteria = example.createCriteria();
        criteria.andStatusEqualTo("2");
        List<TbItem> items = tbItemMapper.selectByExample(example);
        for (TbItem item : items) {

            Map map = JSON.parseObject(item.getSpec(), Map.class);
            item.setSpecMap(map);//   /*{"机身内存":"16G","网络":"联通3G"}转化为Map格式
        }
        solrTemplate.saveBeans(items);
        solrTemplate.commit();
    }


    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath*:spring/applicationContext*.xml");
        SolrUtil solrUtil = context.getBean(SolrUtil.class);
        solrUtil.importItemDate();


    }


}




