package com.pinyougou.task;

import com.pinyougou.mapper.TbSeckillGoodsMapper;
import com.pinyougou.pojo.TbSeckillGoods;
import com.pinyougou.pojo.TbSeckillGoodsExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @program: pinyougou_parent
 * @name: RefreshGoodsAndOrder
 * @author;lin
 * @create: 2019-07-24 15:40
 **/
@Component
public class RefreshGoodsAndOrder {
    /**
     * 刷新秒杀商品
     * 没分钟0秒执行一次,查询一次商品
     * 将符合条件的记录且符合缓存中不存在的商品缓存到redis中
     */
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private TbSeckillGoodsMapper seckillGoodsMapper;

    @Scheduled(cron = "0 * * * * ?")
    public void refreshSeckillGoods() {

        //获得所有秒杀商品的建的集合
        Set keys = redisTemplate.boundHashOps("seckillGoods").keys();
        ArrayList list = new ArrayList(keys);
        //查询数据库秒杀商品
        TbSeckillGoodsExample example = new TbSeckillGoodsExample();
        TbSeckillGoodsExample.Criteria criteria = example.createCriteria();
        criteria.andStatusEqualTo("2");//审核通过
        //开始时间=<当前时间<=结束时间
        Date now = new Date();
        criteria.andStartTimeLessThanOrEqualTo(now);
        criteria.andEndTimeGreaterThanOrEqualTo(now);
        criteria.andStockCountGreaterThan(0);//库存大于0
        criteria.andIdNotIn(list);//排除缓存中已经有的商品
        //数据库的秒杀商品
        List<TbSeckillGoods> tbSeckillGoods = seckillGoodsMapper.selectByExample(example);
        //装入缓存
        for (TbSeckillGoods tbSeckillGood : tbSeckillGoods) {
            redisTemplate.boundHashOps("seckillGoods").put(tbSeckillGood.getId(), tbSeckillGood);
            System.out.println("装入缓存");
        }
    }

    /**
     * 对缓存中过期的商品移除,并存入数据库没秒执行一次
     */
    @Scheduled(cron = "* * * * * ?")
    public void removeSeckillGoods() {
        System.out.println("当前时间" + new Date());

        //查询缓存中的商品
        List<TbSeckillGoods> seckillGoods = redisTemplate.boundHashOps("seckillGoods").values();
        for (TbSeckillGoods seckillGood : seckillGoods) {

            //结束时间<当前时间,表示过期
            if (seckillGood.getEndTime().getTime() < new Date().getTime()) {
                seckillGoodsMapper.updateByPrimaryKeySelective(seckillGood);//更新到数据库
                redisTemplate.boundHashOps("seckillGoods").delete(seckillGood.getId());//从缓存中移除
                System.out.println("移除过期商品");
            }
        }

    }


}




