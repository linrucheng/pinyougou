package com.pinyougou.user.service;
import java.util.List;
import com.pinyougou.pojo.TbUser;

import com.pinyougou.entity.PageResult;
/**
 * 服务层接口
 * @author Administrator
 *
 */
public interface UserService {

	/**
	 * 发送短信验证码
	 * @param phone
	 * @return
	 */
	Boolean createCode(String phone);

	
	/**
	 * 增加
	*/
	public void add(TbUser user);


	public  Boolean checkCode(String phone,String code);
	







}
