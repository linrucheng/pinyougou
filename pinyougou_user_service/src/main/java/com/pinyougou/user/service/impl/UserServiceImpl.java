package com.pinyougou.user.service.impl;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.alibaba.fastjson.JSON;
import com.pinyougou.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.pinyougou.mapper.TbUserMapper;
import com.pinyougou.pojo.TbUser;
import com.pinyougou.pojo.TbUserExample;
import com.pinyougou.pojo.TbUserExample.Criteria;


import com.pinyougou.entity.PageResult;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import javax.jms.*;


/**
 * 服务实现层
 *
 * @author Administrator
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private TbUserMapper userMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 发送短信验证码
     *
     * @param phone
     * @return
     */
    @Autowired
    private JmsTemplate jmsTemplate;//消息队列模板类

    @Autowired
    private Destination queueMessageDestination;//发送的类型

    @Value("${signName}")
    private String signName;
    @Value("${templateCode}")
    private String templateCode;

    @Override
    public Boolean createCode(final String phone) {


        try {
            //6位随机验证码
            final String code = (long) (Math.random() * 1000000) + "";
            //放入缓存 put(phone, code);
            BoundHashOperations phoneAndCode = redisTemplate.boundHashOps("phoneAndCode");
            phoneAndCode.put(phone, code);
            phoneAndCode.expire(3L, TimeUnit.MINUTES);//设置存入redis时间3分钟有效时间


            // final HashMap<String, String> map = new HashMap<>();
        /* map.put("phone", "18096750859");
        map.put("signName", "黑马林百万");
        map.put("templateCode", "SMS_170840048");
        map.put("templateParam", "{ \"code\":\"123456\"}");*/
     /*   map.put("phone",phone);
        map.put("signName",signName);
        map.put("templateCode",templateCode);*/
            //消息生产者
            jmsTemplate.send(queueMessageDestination, new MessageCreator() {
                @Override
                public Message createMessage(Session session) throws JMSException {
                    MapMessage mapMessage = session.createMapMessage();
                    mapMessage.setString("phone", phone);//手机号

                    //对signName解码
                    try {
                        signName = new String(signName.getBytes("iso-8859-1"), "utf-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }


                    mapMessage.setString("signName", signName);//签名
                    mapMessage.setString("templateCode", templateCode);//模板类型
                    //随机生成的验证码
                    Map map = new HashMap();
                    map.put("code", code);
                    String jsonString = JSON.toJSONString(map);
                    mapMessage.setString("templateParam", jsonString);
                    return mapMessage;
                }
            });
            return true;
        } catch (JmsException e) {
            e.printStackTrace();
            return false;
        }


    }


    /**
     * 增加
     */
    @Override
    public void add(TbUser user) {
        userMapper.insert(user);
    }

    /**
     * 判断验证码是否正确
     *
     * @param phone
     * @param code
     * @return
     */
    @Override
    public Boolean checkCode(String phone, String code) {

        String phoneAndCode = (String) redisTemplate.boundHashOps("phoneAndCode").get(phone);
        if (code != null && code.equals(phoneAndCode)) {
            //假如正确,从缓存中移除验证码
            redisTemplate.boundHashOps("phoneAndCode").delete(phone);
            return true;
        } else {
            return false;
        }


    }


}
