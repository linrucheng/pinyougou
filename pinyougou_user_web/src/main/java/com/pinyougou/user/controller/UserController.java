package com.pinyougou.user.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pojo.TbUser;
import com.pinyougou.user.service.UserService;

import com.pinyougou.entity.Result;
import com.pinyougou.entity.PageResult;

/**
 * controller
 *
 * @author Administrator
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Reference(timeout = 5000)
    private UserService userService;

    /**
     * 接收前端的手机号码,调用service
     *
     * @param phone
     * @return
     */
    @RequestMapping("/createCode")
    public Result createCode(String phone) {
        Boolean b = userService.createCode(phone);
        if (b) {
            return new Result(true, "发送成功");
        } else {
            return new Result(false, "发送失败");
        }
    }

    /**
     * 增加
     *
     * @param user
     * @return
     */
    @RequestMapping("/add")
    public Result add(@RequestBody TbUser user, String checkcode) {
        Boolean b = userService.checkCode(user.getPhone(), checkcode);
        if (!b) {
            return new Result(false, "验证码错误");
        }
        try {
            //创建时间
            user.setCreated(new Date());
            //更新时间
            user.setUpdated(new Date());
            //会员来源 1pc
            user.setSourceType("1");
            //密码加密
            String password = DigestUtils.md5Hex(user.getPassword());
            user.setPassword(password);
            userService.add(user);
            return new Result(true, "增加成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "增加失败");
        }
    }

    /**
     * 获取登录的用户名
     *
     * @return
     */
    @RequestMapping("/getUsername")
    public Map getUsername() {
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        Map map = new HashMap();
        map.put("username", name);
        System.out.println(map);
        return map;
    }
}











