//控制层
app.controller('userController', function ($scope, $controller, userService) {

    $controller('baseController', {$scope: $scope});//继承
    $scope.reg = function () {
        if ($scope.entity.password != $scope.password) {
            alert("两次输入的密码不一致")
            $scope.entity.password = '';
            $scope.password = ''
            return;
        }
        //判断手机格式是否正确
        $scope.rg_telephone = /^1[34578]\d{9}$/;
        if (!$scope.rg_telephone.test($scope.entity.phone)) {
            alert("手机格式错误")
            $scope.entity.phone = ''
            return;
        }
        userService.reg($scope.entity, $scope.checkcode).success(function (response) {
            if (response.success) {
                alert("注册成功")
            } else {
                alert(response.message)
            }
        })
    }


    $scope.sendCode = function () {
        //判断手机格式是否正确
        $scope.rg_telephone = /^1[34578]\d{9}$/;
        if (!$scope.rg_telephone.test($scope.entity.phone)) {
            alert("手机格式错误")
            $scope.entity.phone = ''
            return;
        }

        if ($scope.entity.phone == null || $scope.entity.phone == ''||$scope.entity.phone=="undefined") {
            return;
        }
        userService.sendCode($scope.entity.phone).success(
            function (response) {
            })
    }


    //获取用户名
    $scope.getUsername = function () {
        userService.getUsername().success(function (response) {

            $scope.entity=response;
        })}









});