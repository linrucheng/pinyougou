//服务层
app.service('userService', function ($http) {


    //注册
    this.reg = function (entity, checkcode) {
        return $http.post("../user/add.do?checkcode=" + checkcode, entity)
    }
    //验证码
    this.sendCode = function (phone) {
        return $http.post("../user/createCode.do?phone=" + phone)
    }
    //获取用户名
    this.getUsername = function () {
        return $http.post("../user/getUsername.do")
    }


});



